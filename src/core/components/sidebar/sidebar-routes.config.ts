import { IMenuItem } from "./sidebar.metadata";

// Sidebar menu Routes and data
export const MENU_ITEMS: IMenuItem[] = [
    {
        group           : true,
        path            : "https://www.google.com.co",
        title           : "Main",
        class           : "",
    },
    {
        path            : "https://www.google.com.co",
        title           : "Google",
        class           : "",
        icon: {
            class : "fab fa-google",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        isExternalLink  : true,
    },
    {
        path            : "",
        title           : "Builder",
        class           : "has-sub",
        icon: {
            class : "fas fa-wrench",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        submenu: [
            {
                path     : "/courses",
                title    : "Courses",
                class    : "",
                icon: {
                    class : "fab fa-angular",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
        ],
    },
    {
        path            : "",
        title           : "Test",
        class           : "has-sub",
        icon: {
            class : "fas fa-vial",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        submenu: [
            {
                path     : "/home",
                title    : "Home",
                class    : "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path     : "/users",
                title    : "Users",
                class    : "",
                icon: {
                    class : "fas fa-users",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path     : "/chat",
                title    : "Chat",
                class    : "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
        ],
    },
    {
        path: "/thoughts",
        title: "Thoughts",
        class: "",
        icon: {
            class : "fab fa-cloudversify",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        badge: {
            text    : "5",
            color   : "#fff",
            bg      : "#43A047",
        },
    },
    {
        path        : "/movies",
        title       : "Movies",
        class       : "",
        icon: {
            class : "fas fa-film",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
    },
    {
        path        : "/worldcup",
        title       : "Worldcup",
        class       : "",
        icon: {
            class : "fas fa-film",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
    },
    {
        path            : "",
        title           : "Miscellaneous",
        class           : "has-sub",
        icon: {
            class : "fas fa-box-open",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        submenu: [
            {
                path     : "/pets",
                title    : "Pets",
                class    : "",
                icon: {
                    class : "fas fa-paw",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path        : "/orders",
                title       : "Orders",
                class       : "",
                icon: {
                    class : "fas fa-recycle",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path        : "/jokes",
                title       : "Jokes",
                class       : "",
                icon: {
                    class : "far fa-smile",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
        ],
    },
    {
        path            : "",
        title           : "Marvel",
        class           : "has-sub",
        icon: {
            class : "fab fa-rebel",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        submenu: [
            {
                path     : "/heroes",
                title    : "Heroes",
                class    : "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path     : "/heroes",
                title    : "Heroes",
                class    : "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
        ],
    },
    {
        path            : "/weather",
        title           : "Weather",
        class           : "",
        icon: {
            class : "fas fa-umbrella",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
    },
    {
        path: "/tutorial",
        title: "Tutorials",
        class: "",
        icon: {
            class : "fas fa-atlas",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
    },
];
