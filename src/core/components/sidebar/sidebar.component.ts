import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

// import { MENU_ITEMS } from "./sidebar-states.config";
import { MENU_ITEMS } from "./sidebar-routes.config";
import { IMenuItem } from "./sidebar.metadata";
// import { TranslateService } from "@ngx-translate/core";

@Component({
    moduleId    : module.id,
    selector    : "app-sidebar",
    templateUrl : "./sidebar.component.html",
    styleUrls   : [ "./sidebar.component.scss" ],
})

export class SidebarComponent implements OnInit {
    public menuItems: IMenuItem[];

    constructor() {
        //
    }

    public ngOnInit(): void {
        $.getScript( "./assets/js/app-sidebar.js" );
        this.getMenuItems();
    }

    private getMenuItems(): void {
        this.menuItems = MENU_ITEMS.filter( (menuItem) => menuItem );
        console.log( "this.menuItems ----> ", this.menuItems );
    }

    private getStyles( item: any ) {
        return {
            background   : item.bg,
            color        : item.color,
        };
    }

    private toggle( event: Event, item: any, el: any ) {

        event.preventDefault();
        const items: any[] = el.menuItems;

        if ( item.active ) {
            item.active = false;
        } else {

            for (let i of item) {
                i = false;
            }

            // for ( let i = 0; i < items.length; i++ ) {
            //     items[ i ].active = false;
            // }

            item.active = true;
        }
    }
}
