import { IMenuItem } from "./sidebar.metadata";

// Sidebar menu Routes and data
export const MENU_ITEMS: IMenuItem[] = [
    {
        group           : true,
        path            : "https://www.google.com.co",
        title           : "Main",
        class           : "",
    },
    {
        path            : "https://www.google.com.co",
        title           : "Google",
        class           : "",
        icon: {
            class : "fab fa-google",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        isExternalLink  : true,
    },
    {
        path: "/",
        title: "Test",
        class: "has-sub",
        icon: {
            class : "fas fa-vial",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        submenu: [
            {
                path: "home",
                title: "Home",
                class: "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path: "users",
                title: "Users",
                class: "",
                icon: {
                    class : "fas fa-users",
                    bg    : "#ea8080",
                    color : "rgba(0,0,0,.87)",
                },
            },
            {
                path: "chat",
                title: "Chat",
                class: "",
                icon: {
                    class : "fas fa-home",
                    bg    : "#e5e5e5",
                    color : "rgba(0,0,0,.87)",
                },
            },
        ],
    },
    {
        path: "thoughts",
        title: "Thoughts",
        class: "",
        icon: {
            class : "fab fa-cloudversify",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
        badge: {
            text    : "5",
            color   : "#fff",
            bg      : "#43A047",
        },
    },
    {
        path: "home",
        title: "Home",
        class: "",
        icon: {
            class : "fab fa-cloudversify",
            bg    : "#ea8080",
            color : "rgba(0,0,0,.87)",
        },
    },
];
