// Sidebar route metadata
export interface IMenuItem {
    path: string;
    title: string;
    class: string;
    group?: boolean;
    isExternalLink?: boolean;
    badge?: IMenuItemBadge;
    icon?: IMenuItemIcon;
    submenu?: IMenuItem[];
}

export interface IMenuItemIcon {
    class?: string;
    color?: string;
    bg?: string;
}

export interface IMenuItemBadge {
    text?: string;
    color?: string;
    bg?: string;
}
