import { Action } from "@ngrx/store";
import { NavigationExtras } from "@angular/router";

// export interface IRouterMethodCall {
//     path?: string | any[];
//     query?: any;
//     extras?: NavigationExtras;
// }

export const GO = "[Router] -- Go";
export const REPLACE = "[Router] -- Replace";
export const SHOW = "[Router] -- Show";
export const BACK = "[Router] -- Back";
export const FORWARD = "[Router] -- Forward";
export const UPDATE_LOCATION = "[Router] -- Update Location";

export class Go implements Action {
    readonly type = GO;
    constructor(
        public payload: {
            path: any[];
            query?: object;
            extras?: NavigationExtras;
        },
    ) {}
}

export class Replace implements Action {
    readonly type = REPLACE;
    constructor(
        public payload: {
            path: any[];
            query?: object;
            extras?: NavigationExtras;
        },
    ) {}
}

export class Show implements Action {
    readonly type = SHOW;
    constructor(
        public payload: {
            path: any[];
            query?: object;
            extras?: NavigationExtras;
        },
    ) {}
}

export class Back implements Action {
    readonly type = BACK;
}

export class Forward implements Action {
    readonly type = FORWARD;
}

export type Actions = Go | Back | Forward | Replace | Show;
