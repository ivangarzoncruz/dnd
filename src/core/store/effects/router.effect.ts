import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";

import { Effect, Actions } from "@ngrx/effects";
import * as RouterActions from "../actions/router.action";

import { tap, map } from "rxjs/operators";

@Injectable()
export class RouterEffects {

    @Effect({ dispatch: false })
    navigateGo$ = this.actions$.ofType(RouterActions.GO).pipe(
        map((action: RouterActions.Go) => action.payload),
        tap(({ path, query: queryParams, extras }) => {
            // this.router.navigate(path, { queryParams, ...extras });
            this.router.navigate(path, Object.assign({}, extras, { queryParams }));
        }),
    );

    @Effect({ dispatch: false })
    navigateReplace$ = this.actions$.ofType(RouterActions.REPLACE).pipe(
        map((action: RouterActions.Replace) => action.payload),
        tap(({ path, query: queryParams, extras }) => {
            // this.router.navigate(path, { queryParams, ...extras });
            this.router.navigate(path, Object.assign({}, extras, { queryParams, replaceUrl: true }));
        }),
    );

    @Effect({ dispatch: false })
    navigateShow$ = this.actions$.ofType(RouterActions.SHOW).pipe(
        map((action: RouterActions.Show) => action.payload),
        tap(({ path, query: queryParams, extras }) => {
            // this.router.navigate(path, { queryParams, ...extras });
            this.router.navigate(path, Object.assign({}, extras, { queryParams, skipLocationChange: true }));
        }),
    );

    @Effect({ dispatch: false })
    navigateBack$ = this.actions$.ofType(RouterActions.BACK).pipe(
        tap(() =>
            this.location.back(),
        ));

    @Effect({ dispatch: false })
    navigateForward$ = this.actions$.ofType(RouterActions.FORWARD).pipe(
        tap(() =>
            this.location.forward(),
        ));

    constructor(
        private actions$: Actions,
        private router: Router,
        private location: Location,
    ) {}
}
