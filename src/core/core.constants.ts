import { Injectable } from "@angular/core";

@Injectable()
export class Constants {

    public API_ENDPOINT: string;
    public API_KEY: string;
    public CONSUMER_KEY: string;

    public API_SERVER: string;
    public API_PATH: string;
    public BASE_URL: string;

    // public authConfig: object = {
    //     CLIENT_ID   : "AngularFoodClient",
    //     GRANT_TYPE  : "password",
    //     SCOPE       : "WebAPI",
    // };

    // public toasterConfig: ToasterConfig = new ToasterConfig({
    //     positionClass: 'toast-bottom-right'
    // });

    constructor() {
        this.API_SERVER     = "http://localhost:8030";
        this.API_KEY       = "someReallyStupidTextWhichWeHumansCantRead",
        this.API_PATH       = "api";
        this.BASE_URL        = this.API_SERVER + this.API_PATH;
    }
}
