export class Exception extends Error {
    private code: number;

    constructor( statusCode: number, message: string ) {
        super( message );
        this.code = statusCode;
    }

    get statusCode(): number {
        return this.code;
    }

    private toObject(): object {
        return {
            statusCode : this.code,
            message    : this.message,
        };
    }
}
