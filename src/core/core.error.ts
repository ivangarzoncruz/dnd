import { ErrorHandler } from "@angular/core";
import { Exception } from "./core.exception";

export class GlobalErrorHandler implements ErrorHandler {
    public handleError( error: any ): void {
        // const e: Exception = error.rejection.json();
        console.error( error.httpErrorCode );

        const e: Exception = error;
        console.error( e.statusCode + ": " + e.message );
    }
}
