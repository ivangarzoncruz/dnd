import { NgModule, ErrorHandler, Optional, SkipSelf } from "@angular/core";
import { XHRBackend, RequestOptions } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { RouterModule  } from "@angular/router";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireStorageModule } from "angularfire2/storage";
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from "@env/environment";

import { StoreRouterConnectingModule, RouterStateSerializer } from "@ngrx/router-store";
import { StoreModule, MetaReducer } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { reducers, effects, CustomSerializer } from "./store";

// not used in production
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { storeFreeze } from "ngrx-store-freeze";

/** Providers */
import { Constants } from "./core.constants";
import { GlobalErrorHandler } from "./core.error";

/** Components */
import { SidebarComponent } from "./components/sidebar/sidebar.component";

/** Interceptors */
// import { AuthInterceptor } from "./interceptors/auth.interceptor";

// import { LoaderService } from "./../shared/components/loader/loader.service";
// import { LoaderComponent } from "./../shared/components/loader/loader.component";

// this would be done dynamically with webpack for builds
// const env = {
//     development: true,
//     production: false,
// };

import { PagesModule } from "./../pages/pages.module";

export const metaReducers: MetaReducer<any>[] = !environment.production ? [] : [];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule,
        PagesModule,
        StoreModule.forRoot( reducers, { metaReducers } ),
        EffectsModule.forRoot( effects ),
        StoreRouterConnectingModule,
        // environment ? StoreDevtoolsModule.instrument() : [],
        AngularFireModule.initializeApp( environment.firebaseConfig ),
        AngularFireDatabaseModule,
        AngularFireStorageModule,
        AngularFirestoreModule,
    ],
    declarations: [
        SidebarComponent,
        // LoaderComponent,
    ],
    exports: [
        SidebarComponent,
        // LoaderComponent,
    ],
    providers: [
        Constants,
        // LoaderService,
        {
            provide    : ErrorHandler,
            useClass   : GlobalErrorHandler,
        },
        {   provide    : RouterStateSerializer,
            useClass   : CustomSerializer,
        },
        // {
        //     provide    : HTTP_INTERCEPTORS,
        //     useClass   : AuthInterceptor,
        //     multi      : true,
        // },
    ],
})
export class CoreModule {
    /* Make sure CoreModule is imported only by one NgModule the AppModule */
    constructor(
        @Optional() @SkipSelf() parentModule: CoreModule,
    ) {
        if ( parentModule ) {
            throw new Error( `${ parentModule } has already been loaded. Import Core module in the AppModule only.` );
        }
    }
}
