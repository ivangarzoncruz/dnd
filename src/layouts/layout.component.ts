import { Component, OnInit, ElementRef } from "@angular/core";

@Component({
    moduleId    : module.id.toString(),
    selector    : "app-layout",
    templateUrl : "./layout.component.html",
    styleUrls   : [ "./layout.component.scss" ],
})

export class LayoutComponent implements OnInit {
    constructor(private elementRef: ElementRef) { }

    public ngOnInit() {
        // sidebar toggle event listner
    }

    private onClick( event: Event ) {
        // initialize window resizer event on sidebar toggle click event
    }
}
