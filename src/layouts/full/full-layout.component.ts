import { Component, OnInit, ElementRef } from "@angular/core";

// var fireRefreshEventOnWindow = function () {
//     var evt = document.createEvent( "HTMLEvents" );
//     evt.initEvent( "resize", true, false);
//     window.dispatchEvent(evt);
// };

@Component({
    moduleId    : module.id.toString(),
    selector    : "app-full-layout",
    templateUrl : "./full-layout.component.html",
})

export class FullLayoutComponent implements OnInit {
    constructor(private elementRef: ElementRef) { }

    public ngOnInit() {
        // //sidebar toggle event listner
    }

    private onClick( event: Event ) {
        // initialize window resizer event on sidebar toggle click event
    }
}
