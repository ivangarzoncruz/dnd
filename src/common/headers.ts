import { Headers, RequestOptions } from "@angular/http";

let token: string = "AKA82120611069";
let headers: Headers = new Headers({
    // "Access-Control-Allow-Origin": "*",
    // "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH",
    // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
    "Authorization" : "Bearer " + token,
    "Content-Type"  : "application/json",
    "Accept"        : "q=0.8;application/json;q=0.9"
});

export const HttpHeaders: RequestOptions = new RequestOptions({ headers : headers });