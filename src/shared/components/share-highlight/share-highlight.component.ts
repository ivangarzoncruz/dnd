import { Component, OnInit, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import Popper from 'popper.js'; // plugin

import { SharedTooltipService } from './../../services/shared-tooltip.service'

@Component({
    selector    : 'app-share-highlight',
    templateUrl : './share-highlight.component.html',
    styleUrls   : ['./share-highlight.component.scss']
})
export class ShareHighlightComponent implements OnInit {

    private selectedtext : string = '';
    private hostRectangle : object = {};
    private twitterlink : string = '';
    private linkedinlink : string = '';
    private emaillink : string = '';

    private tooltip: any;
    private subscription: Subscription;
    private element: any;
    private active: boolean = false;

    constructor(
        private el: ElementRef,
        private sharedTooltipService: SharedTooltipService
    ) {

        this.element = el.nativeElement;
    }



    // I render the rectangles emitted by the [textSelect] directive.
	public renderRectangles( event: Event ): void {

        console.log( event )

		console.group( "Text Select Event" );
		console.log( "Event:", event );
		// console.log( "Id:", event.id );
		// console.log( "Text:", event.text );
		// console.log( "Viewport Rectangle:", event.viewportRectangle );
		// console.log( "Host Rectangle:", event.hostRectangle );
        console.groupEnd();

        // If a new selection has been created, the viewport and host rectangles will
		// exist. Or, if a selection is being removed, the rectangles will be null.
		// if ( event.hostRectangle ) {

		// 	this.hostRectangle = event.hostRectangle;
		// 	this.selectedText = event.text;

		// } else {

		// 	this.hostRectangle = null;
		// 	this.selectedText = "";

		// }
    }

    // showSelectedText( event: any ) {

    //     let element = event; // this was mostly for testing

    //     var text = "";

    //     if ( window.getSelection ) {

    //         // Get the text that was selected
    //         text = window.getSelection().toString();
    //         console.log( 'text ----> ', text );

    //         if ( text != "" ) {

    //             // See where the selection is and attach popper to it
    //             // const selection = window.getSelection().getRangeAt(0);

    //             this.element.selection = window.getSelection().getRangeAt(0);
    //             console.log( 'selection ----> ', this.element.selection );

    //             /* Each item in the ranges array is now
    //              * a range object representing one of the
    //              * ranges in the current selection */

    //             // Setting up the tooltip (popper)
    //             let popper = document.querySelector( '.js-popper' );

    //             new Popper( this.element, popper, {
    //                 placement: 'top'
    //             });

    //             // Show popper
    //             this.showStyle = true;

    //         } else {

    //             // Hide popper
    //             this.showStyle = false;
    //         }

    //     } else {
    //         this.showStyle = false;
    //     }
    // }

    getStyle() {
        if( this.active ) {
            return "block";
        } else {
            return "none";
        }
    }

    ngOnInit() {
        // subscribe to home component messages
        this.subscription = this.sharedTooltipService.getMessage()
            .subscribe( tooltip => {

                this.tooltip = tooltip;
                console.log( 'tooltip ---> ', this.tooltip );

                // this.setTooltip( this.tooltip );

                this.active =  true;
                // this.selectedtext = this.tooltip.text;
                // this.hostRectangle = this.tooltip.viewportRectangle;
            });
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    setTooltip( tooltip: any ) {

        console.log( 'tooltip ---> ', tooltip );

        // this.tooltip = tooltip;

        this.active =  true;
        this.selectedtext = tooltip.text;
        this.hostRectangle = tooltip.viewportRectangle;

        console.log( 'hostRectangle ---> ', this.hostRectangle );

        // Building the share links with highlighted text and additional info you might want to add
        this.twitterlink = "https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.WebsiteLinkHere.com&text=" + this.selectedtext.split(' ').join('%20') + "%20via%20@TwitterHandle ";
        this.linkedinlink = "https://www.linkedin.com/shareArticle?mini=true&url=http://www.WebsiteLinkHere.com&title=" + ("Title here.").split(' ').join('%20') + "&summary=" + this.selectedtext.split(' ').join('%20') + "&source=SourceHere";
        this.emaillink = "mailto:?subject=" + ("Email subject line here.").split(' ').join('%20') + "&body=" + this.selectedtext.split(' ').join('%20') + (". Some additional text here if you want, http%3A%2F%2Fwww.WebsiteLinkHere.com.").split(' ').join('%20');
    }

}
