import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { getPaginationModel, ITEM_TYPES } from "ultimate-pagination";

@Component({
    selector       : "app-pagination",
    templateUrl    : "./pagination.component.html",
    styleUrls      : [ "./pagination.component.css" ],
})
export class PaginationComponent implements OnInit {

    @Input()
    private currentPage: number;

    @Input()
    private totalPages: number;

    @Output()
    private change = new EventEmitter();
    private ITEM_TYPES = ITEM_TYPES;

    constructor() {
        //
    }

    public ngOnInit() {
        //
    }

    private getPaginationModel() {
        return getPaginationModel({
            currentPage: this.currentPage,
            totalPages: this.totalPages,
        });
    }

    private onSelectPage( newPage: number ) {
        if ( newPage !== this.currentPage ) {
            this.change.next( newPage );
        }
    }

    private getItemKey( index: number, item: any ) {
        return item.key;
    }
}
