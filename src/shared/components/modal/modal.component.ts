import { Component, ElementRef, Input, OnInit, OnDestroy, HostBinding, Inject, ViewChild, Renderer2 } from "@angular/core";
import { DOCUMENT } from '@angular/common';

import { ModalService } from "@shared/services/modal.service";

@Component({
    moduleId    : module.id,
    selector    : "app-modal",
    templateUrl : "./modal.component.html",
    styleUrls   : [ "./modal.component.scss" ],
})

export class ModalComponent implements OnInit, OnDestroy {

    @Input()
    private id: string;
    private element: any;
    public show: boolean = false;

    @ViewChild( 'modalWindow' )
    public modalWindow: ElementRef;

    // alternatively also the host parameter in the @Component()` decorator can be used
    @HostBinding( 'class.someClass' ) someField: boolean = false;

    constructor(
        private modalService: ModalService,
        private el: ElementRef,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private document: Document
    ) {


        this.element = el.nativeElement;
    }



    ngOnInit(): void {

        const modal = this;

        // ensure id attribute exists
        if ( !this.id ) {
            console.error( 'modal must have an id' );
            return;
        }

        // move element to bottom of page (just before </body>) so it can be displayed above everything else
        this.document.body.appendChild( this.element );

        // close modal on background click
        this.element.addEventListener( "click", function ( e: any ) {
            if ( e.target.className === "app-modal-show" ) {
                modal.close();
            }
        });

        // Click outside of the modal and close it
        // window.onclick = function(e) {
        //     if ( e.target == modal ) {
        //         close();
        //     }
        // }

        // Use the escape key to close modal
        this.document.onkeyup = function( e: any ) {

            e = e || window.event;

            // if( el.nativeElement.classList.contains( 'app-modal-show')) {
            if ( e.keyCode == 27 ) {
                modal.close();
            }
            // }
        }

        // add self (this modal instance) to the modal service so it"s accessible from controllers
        this.modalService.add( modal );
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove( this.id );
        this.element.remove();
    }

    // open modal
    open(): void {

        this.renderer.addClass( this.modalWindow.nativeElement, 'app-modal-show' );
        this.renderer.addClass( this.document.body, 'block-scroll' );
        // this.renderer.addClass( this.document.body, 'blur-filter' );
        // this.document.body.classList.add( 'app-modal-open' );

    }

    // close modal
    close(): void {

        this.renderer.removeClass( this.modalWindow.nativeElement, 'app-modal-show');
        this.renderer.removeClass( this.document.body, 'block-scroll' );
        // this.renderer.removeClass( this.document.body, 'blur-filter' );

        // this.renderer.addClass( this.modalWindow.nativeElement, 'app-modal-hide' );
        // This listens for the CSS animations to finish and then hides the modal
        // this.modalWindow.nativeElement.addEventListener( "webkitAnimationEnd", this.afterAnimation, false );
        // modal.addEventListener("oAnimationEnd", afterAnimation, false);
        // modal.addEventListener("msAnimationEnd", afterAnimation, false);
        // modal.addEventListener("animationend", afterAnimation, false);
    }

    // document.body.classList.remove( "app-modal-open" );
    // Remove hide class after animation is done
    afterAnimation (): void {
        console.log( 'afterAnimation' );
        this.renderer.removeClass( this.modalWindow.nativeElement, 'app-modal-hide');
    }
}
