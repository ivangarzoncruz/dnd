import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";

import { ILoader as Loader } from "./loader.model";

@Injectable()
export class LoaderService {

    public loaderSubject = new Subject<Loader>();
    public loaderState = this.loaderSubject.asObservable();

    constructor() {
        //
    }

    public show(): void {
        this.loaderSubject.next(<Loader>{show: true});
    }

    public hide(): void {
        this.loaderSubject.next(<Loader>{show: false});
    }
}
