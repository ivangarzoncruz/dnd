import { Component, Input } from "@angular/core";

import { Subscription } from "rxjs/Subscription";

import { LoaderService } from "./loader.service";
import { ILoader as Loader } from "./loader.model";

@Component({
    moduleId       : module.id,
    selector       : "app-loader",
    templateUrl    : "./loader.component.html",
    styleUrls      : [ "./loader.component.scss" ],
})
export class LoaderComponent {
    @Input()
    private loading: boolean;

    @Input()
    private failed: boolean;

    @Input()
    private show: boolean;

    private subscription: Subscription;

    constructor(private loaderService: LoaderService) {
        this.loading = false;
        this.failed = false;
        this.show = false;
    }

    private ngOnInit(): void {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: Loader) => {
                this.show = state.show;
            });
    }

    private ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
