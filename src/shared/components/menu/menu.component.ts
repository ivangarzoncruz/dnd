import { Component, OnInit } from "@angular/core";

@Component({
    moduleId    : module.id.toString(),
    selector    : "app-menu",
    templateUrl : "./menu.component.html",
})
export class MenuComponent {}
