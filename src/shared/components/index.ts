import { FooterComponent } from "./footer/footer.component";
import { PaginationComponent } from "./pagination/pagination.component";
import { ModalComponent } from "./modal/modal.component";
import { ShareHighlightComponent } from './share-highlight/share-highlight.component';
// import { LoaderComponent } from "./components/loader/loader.component";

export const components: any[] = [
    FooterComponent,
    PaginationComponent,
    ModalComponent,
    ShareHighlightComponent,
    // LoaderComponent,
];

export * from "./footer/footer.component";
export * from "./pagination/pagination.component";
export * from "./modal/modal.component";
export * from "./share-highlight/share-highlight.component";
// export * from "./components/loader/loader.component";
