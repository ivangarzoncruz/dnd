import { Component, Inject, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
    moduleId    : module.id.toString(),
    selector    : "scroll-up",
    templateUrl : "./scroll-up.component.html",
    styleUrls   : [ "./scroll-up.component.scss".toString() ],
})
export class ScrollUpComponent {

    public showButton: boolean;

    constructor(@Inject(DOCUMENT) private document: Document) {
        this.showButton = false;
    }

    @HostListener("window:scroll", [])
    onWindowScroll(): void {
        const scrollTopHeight = document.body.scrollTop || 0;

        if (scrollTopHeight > 200) {
            this.showButton = true;
        } else  {
            this.showButton = false;
        }
    }

    public scrollToTop(): void {
        this.document.body.scrollTop = 0;
    }
}
