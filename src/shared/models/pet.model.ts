export interface IPet {
    id: number;
    name: string;
    description?: string;
    avatar: string;
}
