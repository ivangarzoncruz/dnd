// export interface IHero {
//   id: number;
//   name: string;
//   description?: string;
//   avatar?: string;
//   first_appereance?: string;
// }

export interface IHero {
    id: number;
    name: string;
    description: string;
    thumbnail: IHeroThumbnail[];
}

export class IHeroThumbnail {
    path: string;
    extension: string;
}
