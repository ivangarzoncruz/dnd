export interface ITeams {
    id: number;
    country: string;
    alternate_name?: null;
    fifa_code: string;
    group_id: number;
    group_letter: string;
}
