export interface IThought {
    id?: number;
    name: string;
    thought: string;
    user: IThoughtUser[];
}

export interface IThoughtUser {
    text?: string;
    color?: string;
    bg?: string;
}
