// import { IHero as Hero } from "./hero.model";

// export interface IMarvel {
//     code: number;
//     status: string;
//     attributionHTML: string;
//     data: IMarvelList[];
// }

// export interface IMarvelList {
//     offset: number;
//     limit: number;
//     total: number;
//     count: number;
//     results: IHero[];
// }

// export interface IMarvelListResults {
//     id?: string;
//     name?: string;
//     description?: string;
// }

// export interface IHero {
//     id: number;
//     name: string;
//     description: string;
//     thumbnail: IHeroThumbnail[];
// }

// export class IHeroThumbnail {
//     path?: string;
//     extension?: string;
// }

export interface IMarvel {
    code: number;
    status: string;
    copyright: string;
    attributionText: string;
    attributionHTML: string;
    etag: string;
    data?: IMarvelData[];
}

export interface IMarvelData {
    offset: number;
    limit: number;
    total: number;
    count: number;
    results?: IDataResults[] | null;
}

export interface IDataResults {
    id: number;
    name: string;
    description: string;
    modified: string;
    thumbnail: IHeroThumbnail[];
    resourceURI: string;
    comics: IHeroComicsOrSeriesOrEvents[];
    series: IHeroComicsOrSeriesOrEvents;
    stories: IHeroStories;
    events: IHeroComicsOrSeriesOrEvents;
    urls?: IHeroUrlsEntity[] | null;
}

export interface IHeroThumbnail {
    path: string;
    extension: string;
}

export interface IHeroComicsOrSeriesOrEvents {
    available: number;
    collectionURI: string;
    items?: IHeroItems[] | null;
    returned: number;
}

export interface IHeroItems {
    resourceURI: string;
    name: string;
}

export interface IHeroStories {
    available: number;
    collectionURI: string;
    items?: IStoriesItems[] | null;
    returned: number;
}

export interface IStoriesItems {
    resourceURI: string;
    name: string;
    type: string;
}

export interface IHeroUrlsEntity {
    type: string;
    url: string;
}
