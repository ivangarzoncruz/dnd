export interface Upload {
    key?: string;
    name: string;
    url?: string;
    file: File;
    progress?:number;
    created_at?: string;
}
