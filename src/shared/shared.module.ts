import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule  } from "@angular/router";

/** IMPORT COMPONENTS */
import * as fromComponents from './components';

/** IMPORT DIRECTIVES */
import * as fromDirectives from './directives';

/** IMPORT PIPES */
import * as fromPipes from './pipes'

/** IMPORT SERVICES */
import * as fromServices from './services';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
    ],
    declarations: [
        ...fromComponents.components,
        ...fromDirectives.directives,
        ...fromPipes.pipes,
    ],
    exports: [
        ...fromComponents.components,
        ...fromDirectives.directives,
        ...fromPipes.pipes,
        // HighlightDirective,
    ],
})
export class SharedModule {
    static forRoot() : ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [ ...fromServices.services ]
        };
    }
 }
