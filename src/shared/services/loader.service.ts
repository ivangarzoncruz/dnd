import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
import "rxjs/add/operator/do";

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

    constructor( private loadingBar: SlimLoadingBarService ) {
    }

    intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        // start our loader here
        this.loadingBar.start();

        return next
            .handle(req)
            .do((event: HttpEvent<any>) => {
                // if the event is for http response
                if (event instanceof HttpResponse) {
                // stop our loader here
                this.loadingBar.complete();
            }

        }, (err: any) => {
            // if any error (not for just HttpResponse) we stop our loader bar
            this.loadingBar.complete();
        });
    }
}
