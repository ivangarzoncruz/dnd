import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedTooltipService {

    private subject = new Subject<any>();

    sendMessage( tooltip: any ) {
        console.group( "Text Select Event" );
        console.log( "Id:", tooltip.id );
        console.log( "Text:", tooltip.text );
        console.log( "Viewport Rectangle:", tooltip.viewportRectangle );
        console.log( "Host Rectangle:", tooltip.hostRectangle );
        console.groupEnd();
        this.subject.next( tooltip );
    }

    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
