import { ModalService } from './modal.service';
import { UploadService } from './upload/upload.service';

export const services: any[] = [
    ModalService,
    UploadService,
];

export * from  './modal.service';
export * from './upload/upload.service';
