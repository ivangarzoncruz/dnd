import { Injectable } from "@angular/core";

@Injectable()
export class StorageService {

    private storage: Storage;

    constructor() {
        this.storage = localStorage;
    }

    public setItem = ( key: string, value: any ): void => {
        this.storage.setItem( key, JSON.stringify( value ) );
    }

    public removeItem = ( key: string ): void => {
        this.storage.removeItem( key );
    }

    public getItem = ( key: string ): any => {
        const item: any = this.storage.getItem( key );

        if ( item && item !== "undefined" ) {
            return JSON.parse( this.storage.getItem( key ) );
        }

        return;
    }
}
