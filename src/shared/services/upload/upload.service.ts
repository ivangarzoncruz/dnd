import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';


import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { Upload } from "@shared/models/upload.model"

export interface Image {
    id: string;
    type: string;
    format: string;
    size: number;
    path: string;
    url: string;
    name: string;
    created: number;
}

@Injectable()
export class UploadService {

    constructor(
        private af: AngularFireStorage,
        private db: AngularFireDatabase,
        private afs: AngularFirestore,
    ) { }

    private basePath:string = '/uploads/courses';

    public imgName: string;
    public imgType: string;
    public imgFormat: string;
    public imgSize: number;
    public imgPath: string;

    ref: AngularFireStorageReference;
    task: AngularFireUploadTask;
    uploadState: Observable<string>;
    uploadProgress: Observable<number>;
    downloadURL: Observable<string>;

    public uploadList: AngularFireList<Upload>;

    pushUpload( upload: any ) {

        // const id = Math.random().toString( 36 ).substring( 2 );

        // client side validation
        if ( upload.file.type.split( '/' )[0] !== 'image') {
            console.error( 'unsupported file type!' );
        }

        this.imgName     = upload.name;
        this.imgType     = upload.file.type.split( '/' )[0];
        this.imgFormat   = upload.file.type.split( '/' )[1];
        this.imgSize     = upload.size;

        this.imgPath = `${ this.basePath }/${ this.imgName }`;

        this.ref = this.af.ref( this.imgPath );
        this.task = this.ref.put( upload.file );

        this.uploadProgress = this.task.percentageChanges();

        // get notified when the download URL is available
        this.task.snapshotChanges()
            .pipe(
                finalize(() => {
                    this.downloadURL = this.ref.getDownloadURL();
                    this.downloadURL.subscribe( data => {

                        const image: Image = {
                            id      : this.afs.createId(),
                            url     : data,
                            type    : this.imgType,
                            format  : this.imgFormat,
                            size    : this.imgSize,
                            path    : this.imgPath,
                            name    : this.imgName,
                            created : Date.now(),
                        };

                        this.saveFileData( image );

                        // image object inserted in image collection (AngularFirestoreCollection)
                        // this.imagesCollection.doc(id).set(image);

                        // setting the image name back to blank
                        // this.imageNm = '';

                        // this.upload.emit({
                        //     avatar: image,
                        // });

                    });
                })
            )
            .subscribe();
    }

    getFileUploads( numberItems: number ): AngularFireList<Image> {

        // this.imagesCollection = this.afs.collection<Image>( 'uploads', ref => ref.orderBy( 'created', 'desc' ));
        // this.images = this.imagesCollection.valueChanges();


        return this.db.list( 'uploads', ref => ref.orderByChild( 'created' ).limitToLast( numberItems ));
    }

    // Writes the file details to the realtime db
    saveFileData( upload: Image ) {
        this.db.list( 'uploads' ).push( upload );
    }

    /*
    getFileUploads( numberItems: number ): AngularFireList<FileUpload> {
        return this.db.list( this.basePath, ref =>
            ref.limitToLast(numberItems));
    }

    deleteFileUpload( fileUpload: FileUpload ) {
        this.deleteFileDatabase( fileUpload.key )
        .then(() => {
            this.deleteFileStorage( fileUpload.name );
        })
        .catch( error => console.log(error));
      }

    private deleteFileDatabase( key: string ) {
        return this.db.list(`${this.basePath}/`).remove( key );
    }

    private deleteFileStorage( name: string ) {
        const storageRef = firebase.storage().ref();
        storageRef.child(`${this.basePath}/${name}`).delete();
    }
    */

}
