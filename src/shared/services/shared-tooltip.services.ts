export class SharedTooltipService {

    private tooltips: any[] = [];

    add( tooltip: any ) {
        // add modal to array of active modals
        this.tooltips.push( tooltip );
    }

    remove( id: string ) {
        // remove modal from array of active modals
        this.tooltips = this.tooltips.filter( ( x ) => x.id !== id);
    }

    open( id: string ) {
        // open modal specified by id
        const tooltip: any = this.tooltips.filter( ( x ) => x.id === id)[0];
        tooltip.open();
    }

    close( id: string ) {
        // close modal specified by id
        const tooltip: any = this.tooltips.filter( ( x ) => x.id === id)[0];
        tooltip.close();
    }
}
