import { Router } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthService {

    private token: string;

    constructor() { }

    signupUser( email: string, password: string ): void {
        // your code for signing up the new user
    }

    signinUser( email: string, password: string ): void {
        // your code for checking credentials and getting tokens for for signing in user
    }

    logout(): void {
        this.token = null;
    }

    getToken(): string {
        return this.token;
    }

    isAuthenticated(): boolean {
        // here you can check if user is authenticated or not through his token,
        return true;
    }
}
