import { ReversePipe } from './reverse.pipe';

export const pipes: any[] = [
    ReversePipe,
];

export * from  './reverse.pipe';
