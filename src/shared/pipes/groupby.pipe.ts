import { Pipe, PipeTransform } from "@angular/core";
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength : exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({
    name: "groupBy",
})
export class GroupByPipe implements PipeTransform {
    transform( collection: any, property: string ): any {
        // prevents the application from breaking if the array of objects doesn't exist yet
        if ( !collection ) {
            return null;
        }

        const groupedCollection = collection.reduce(( previous: any, current: any ) => {
            if (!previous[ current[ property ]]) {
                previous[ current[ property ]] = [ current ];
            } else {
                previous[ current[ property ]].push( current );
            }

            return previous;
        }, {});

        // this will return an array of objects, each object containing a group of objects
        return Object.keys(groupedCollection).map( (key) => ({ key, value : groupedCollection[ key ] }));
    }
}
