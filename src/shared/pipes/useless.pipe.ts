import { Pipe, PipeTransform } from "@angular/core";

/*
 * Adds a string before and after to
 * the provided string
 * Usage:
 *   value | usless : before : after
 * Example:
 *   {{ Ivancho | useless : "Mr." : "the great" }}
 *   formats to: Mr. Ivancho the great
*/
@Pipe({
    name: "useless",
})
export class UselessPipe implements PipeTransform {
    transform( value: string, before: string, after: string ): string {
        const str: string = `${before} ${value} ${after}`;
        return str;
    }
}
