import { Pipe, PipeTransform } from "@angular/core";
/**
 * `powerTo` pipe: raises the base to the given exponent.
 * @param: base {number}
 * @param: exponent {number}, default = 1
 * @usage:
 *  template.html
 *  `value is {{ 2 | powerTo:4}}` -> value is 4.
 */
@Pipe({
    name : "powerTo",
})
export class PowerPipe implements PipeTransform {
    transform( base: number, args: number ) {
        const exponent: number = args || 1;
        return Math.pow( base, exponent );
    }
}
