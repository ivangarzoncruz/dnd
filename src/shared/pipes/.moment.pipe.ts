import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({
    name: "moment"
})
export class MomentPipe implements PipeTransform {
  transform( value: number, args: string[] ): any {

    let date = moment( value );

    if ( date.isValid() ) {
      return date.format( args[0] || "L" );
    } else {
      return value;
    }
  }

  // transform( value: any, ...args: string[] ): any {
  //   if ( typeof value === 'string') {
  //     value = +value;
  //   }

  //   return moment.unix( value );
  // }
}