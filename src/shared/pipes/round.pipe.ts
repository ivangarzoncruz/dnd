import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "round",
})
export class RoundPipe implements PipeTransform {
    transform ( input: number) {
        if ( typeof input !== "number" ) {
            throw new Error( input );
        }

        return Math.floor( input );
    }
}