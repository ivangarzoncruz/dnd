import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "lastnameUppercase",
})
export class LastnameUppercase implements PipeTransform {
    transform( v: string, args: any ) {
        return `${v.split(" ")[0]} ${v.split(" ")[1].toUpperCase()}`;
    }
}
