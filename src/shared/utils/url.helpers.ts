export function getQueryParam( prop: any ): any {
    const params = {};
    const search = decodeURIComponent(window.location.href.slice(window.location.href.indexOf( "?" ) + 1));
    const definitions = search.split( "&" );

    for (let param of definitions) {
        let parts = param.split( "=", 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    }

    return ( prop && prop in params ) ? params[ prop ] : params;
}
