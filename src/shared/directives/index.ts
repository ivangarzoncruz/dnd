import { HighlightDirective } from './highlight/highlight.directive';
import { ShareHighlightDirective } from './share-highlight/share-highlight.directive';
import { ShadowDirective } from './shadow/shadow.directive';

export const directives: any[] = [
    ShareHighlightDirective,
    HighlightDirective,
    ShadowDirective,
];

export * from  './highlight/highlight.directive';
export * from './share-highlight/share-highlight.directive';
export * from './shadow/shadow.directive';
