/* tslint:disable:no-unused-variable */

import { TestBed, async } from "@angular/core/testing";
import { FontSizeDirective } from "./font-size.directive";

describe("Directive: FontSize", () => {
  it("should create an instance", () => {
    const directive = new FontSizeDirective();
    expect(directive).toBeTruthy();
  });
});
