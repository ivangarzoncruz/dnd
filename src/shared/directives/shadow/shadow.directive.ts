import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

/**
 * @example <p [appShadow]="'hotpink'"
 *             [appShadowX]="'12px'"
 *             [appShadowY]="'6px'"
 *             [appShadowBlur]="'30px'">Text in shadow</p>
 *
 */
@Directive({
    selector: '[appShadow]',
})
export class ShadowDirective {

    @Input() appShadow: string;
    @Input() appShadowX: string;
    @Input() appShadowY: string;
    @Input() appShadowBlur: string;

    private element: HTMLElement;
    private renderer: Renderer2;

    constructor( el: ElementRef, renderer: Renderer2 ) {
        this.element = el.nativeElement;
        this.renderer = renderer;
        // this.renderer.setStyle( el.nativeElement, 'box-shadow', '2px 2px 12px #58A362' );
    }

    @HostListener( 'mouseenter' ) onMouseEnter() {
        this.element.style.fontStyle = 'italic';
    }

    @HostListener( 'mouseleave' ) onMouseLeave() {
        this.element.style.fontStyle = 'normal';
    }

    ngOnInit() {
        let shadowStr = `${ this.appShadowX } ${ this.appShadowY } ${ this.appShadowBlur } ${ this.appShadow }`;
        this.renderer.setStyle( this.element, 'box-shadow', shadowStr );
    }
}
