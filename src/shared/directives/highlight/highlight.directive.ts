import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
      selector: '[appHighlight]'
})
export class HighlightDirective {

    @Input() defaultColor: string;
    @Input( 'appHighlight' ) highlightColor: string;

    private element: HTMLElement;

    constructor( private el: ElementRef ) {
        this.element = el.nativeElement;
    }

    @HostListener( 'mouseenter' ) onMouseEnter( event: Event  ): void {
        this.highlight( this.highlightColor || this.defaultColor || 'cyan' );
    }

    @HostListener( 'mouseleave' ) onMouseLeave( event: Event ) {
        this.highlight( null );
    }

    private highlight( color: string ) {
        this.element.style.backgroundColor = color;
    }

    // ngOnInit(): void {
        // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        // Add 'implements OnInit' to the class.
        // console.log( '^_^' );
    // }
}

