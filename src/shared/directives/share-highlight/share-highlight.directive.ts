import { Directive, ElementRef, HostListener, Input, EventEmitter, OnInit, Output } from "@angular/core";
import { SharedTooltipService } from './../../services/shared-tooltip.service'
import * as Popper from 'popper.js'; // plugin

export interface TextSelectEvent {
    id: number;
	text: string;
	viewportRectangle: SelectionRectangle | null;
}

interface SelectionRectangle {
	left: number;
	top: number;
	width: number;
	height: number;
}

@Directive({
    selector: '[appSharehighlight]'
})
export class ShareHighlightDirective {

    private id: number;
    private element: HTMLElement;
    private eventTypes = [ "selectionchange", "mouseup", "touchend", "touchcancel" ];

   constructor( private el: ElementRef, private sharedTooltipService: SharedTooltipService ) {

        this.element = el.nativeElement;
        // this._document = document;
        // this._window = this._document.defaultView;
    }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.

        // if ( !window.getSelection ) {
        //     console.warn( "share-this: Selection API isn't supported" );
        //     return;
        // }

        // for( let event in this.eventTypes ) {
        //     this.addListener( this.eventTypes[ event ] );
        // }
    }

    addListener( type: string ) {
        console.log( 'type ----> ', type );
        document.addEventListener( type, this.selectionCheck );
    }

    selectionCheck( evt: Event ) {

        const type = evt.type;
        const shouldHavePopover = type === "mouseup";

        // console.log( 'evt ---> ', evt );

        if ( shouldHavePopover ) {
            // Safari iOS fires selectionchange *before* click, so tapping on a sharer would be prevented
            setTimeout(function () {
                const range = this.getConstrainedRange();
                if ( range ) {
                    //do something
                    console.log( 'range ------> ', range );

                    // var _document = options.document;
                    // var _window = _document.defaultView;
                    // var selection = _window.getSelection();
                    // var isForward = isSelectionForward(selection);
                    // var endLineRect = getEndLineRect(range, isForward);
                    // var offsetScroll = getOffsetScroll(_window);


                }
            }.bind( this ), 10);
        }
    }

    @HostListener( 'mouseup', ['$event'] ) onMouseUp( event: Event ) {
        console.log('mouseup');
        this.shareHighlight();
    }

    // @HostListener( 'document:selectionchange', ['$event'] ) onSelectionChange( event: Event ) {
    //     console.log('selectionchange');
    //     this.selectionCheck( event );
    // }


    // @HostListener( 'change', ['$event'] ) onSelectionChange( event: Event ): void {
    //     // this.shareHighlight();
    //     // this.getSelectedText();
    //     console.log( 'selectionchange' );
    // }

    // @HostListener( 'mouseup', ['$event']) onListenerTriggered( event: any ) : void {
    //     console.log( 'focusout' );
    // }

    // @HostListener( 'mouseup', ['$event'] ) onMouseUp( event: Event ) {
    //     // this.shareHighlight();
    //     this.getSelectedText();
    // }

    // @HostListener( 'mouseenter', ['$event'] ) onMouseEnter( event: Event ) {
    //     // this.shareHighlight();
    //     this.getSelectedText();
    // }

    // private  getSelectedText() {
    //     const range = this.getConstrainedRange();
    //     if ( range ) {
    //         //do something
    //     }
    // }

    getConstrainedRange() {

        const selection = window.getSelection();
        const range = selection.rangeCount && selection.getRangeAt(0);
        if (!range) return;

        const constrainedRange = this.constrainRange( range, 'body' );
        if ( constrainedRange.collapsed || !constrainedRange.getClientRects().length ) return;

        return constrainedRange;
    }

    constrainRange( range: any, selector: any ) {

        const constrainedRange = range.cloneRange();

        if ( range.collapsed || !selector ) {
            return constrainedRange;
        }

        let ancestor = this.closest( range.startContainer, selector );
        // console.log( 'ancestor ----> ', ancestor );

        if ( ancestor ) {
            if ( !this.contains( ancestor, range.endContainer )) {
                constrainedRange.setEnd( ancestor, ancestor.childNodes.length );
            }
        } else {
            ancestor = this.closest( range.endContainer, selector );
            if ( ancestor ) constrainedRange.setStart( ancestor, 0 );
            else constrainedRange.collapse();
        }

        return constrainedRange;
    }

    closest( element: any, selector: any ) {
        let target = element;
        while (target && ( target.nodeType !== 1 /* === Node.ELEMENT_NODE */ || !this.matches(target, selector) )) {
            target = target.parentNode;
        }

        return target;
    }

    // `contains` in IE doesn't work with text nodes
    contains( ancestor: any, target: any ) {
        const comparedPositions = ancestor.compareDocumentPosition( target );
        return !comparedPositions || ( comparedPositions & 16 /* === Node.DOCUMENT_POSITION_CONTAINED_BY */ ) > 0;
    }

    matches( element: any, selector: any ) {

        let matchFunc;

        if ( !matchFunc ) {
            matchFunc = this.getMatchFunctionName( element );
        }
        return element[ matchFunc ](selector);
    }

    getMatchFunctionName( element: any ) {
        const suffix = "atchesSelector";
        for ( const name of [ "matches", `m${suffix}`, `webkitM${suffix}`, `mozM${suffix}`, `msM${suffix}`, `oM${suffix}` ] ) {
            if ( element[ name ] ) {
                return name;
            }
        }
    }


    shareHighlight() {

        let text = '';

        // If the new selection is empty (for example, the user just clicked somewhere
        // in the document), then there's no new selection event to emit.

        console.log( 'window.getSelection -----> ', window.getSelection );

        if ( window.getSelection ) {

            // Get the text that was selected
            text = window.getSelection().toString();


            if ( text != "" ) {

                // See where the selection is and attach popper to it
                // const content = window.getSelection().getRangeAt(0);

                const selection = window.getSelection();
                const range = selection.rangeCount && selection.getRangeAt(0);

                // I get the deepest Element node in the DOM tree that contains the entire range.
                let container = range.commonAncestorContainer;

                while ( container.nodeType !== Node.ELEMENT_NODE ) {
                    container = container.parentNode;
                }

                // We only want to emit events for selections that are fully contained within the
        		// host element. If the selection bleeds out-of or in-to the host, then we'll
        		// just ignore it since we don't control the outer portions.
        		if ( this.element.contains( container ) ) {
                    const viewportRectangle = range.getBoundingClientRect();
                    // const viewportRectangle = range.getClientRects();
                    // const localRectangle = this.viewportToHost( viewportRectangle, container );

                    const host = this.element;
                    const hostRectangle = host.getBoundingClientRect();

                    // Both the selection rectangle and the host rectangle are calculated relative to
                    // the browser viewport. As such, the local position of the selection within the
                    // host element should just be the delta of the two rectangles.
                    let localLeft = ( viewportRectangle.left - hostRectangle.left );
                    let localTop = ( viewportRectangle.top - hostRectangle.top );

                    let node = container;

                    do {

                        localLeft += ( <Element>node ).scrollLeft;
                        localTop += ( <Element>node ).scrollTop;

                    } while ( ( node !== host ) && ( node = node.parentNode ) );

                    const localRectangle = {
                        left: localLeft,
                        top: localTop,
                        width: viewportRectangle.width,
                        height: viewportRectangle.height
                    };

                    // Show tooltip
                    const tooltip = {
                        id    : Math.random(),
                        text  : range.toString(),
                        viewportRectangle: {
                            'left':    viewportRectangle.left,
                            'top':     viewportRectangle.top,
                        },
                        // hostRectangle: {
                        //     'left':     localRectangle.left,
                        //     'top':      localRectangle.top,
                        //     'width':    localRectangle.width,
                        //     'height':   localRectangle.height
                        // }
                    };

                    // send message to subscribers via observable subject
                    this.sharedTooltipService.sendMessage( tooltip );


                }



            } else {

                // Hide popper
                this.destroy();
                console.log( 'destroy 1' );
            }

        } else {
            // Hide tooltip
            // this.destroy();
            console.log( 'destroy' );
        }
    }

    private extend( dest: any, source: any ) {
        if (source && typeof source === "object") {
            // eslint-disable-next-line guard-for-in
            for (const prop in source) {
                // eslint-disable-next-line no-param-reassign
                dest[prop] = source[prop];
            }
        }

        return dest;
    }

    ngOnDestroy(): void {
        // Hide tooltip
        this.destroy();
    }

    destroy(): void {

        this.sharedTooltipService.clearMessage();

        // const idx = this.tooltipService.components.findIndex( ( t ) => {
        //     return t.id === this.id;
        // });

        // this.tooltipService.components.splice( idx, 1 );
    }
}
