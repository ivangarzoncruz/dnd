export const environment = {
    production: false,
    hmr: true,
    apiURL: "api/",
    apiHost: "https://api.somedomain.com/dev/v1/",
    codes: [ "AB", "AC", "XYZ" ],
    firebaseConfig : {
        apiKey: "AIzaSyDS6yMV_cVvAZsD311PE7Z2U4xNQxaKa3Q",
        authDomain: "angular-many.firebaseapp.com",
        databaseURL: "https://angular-many.firebaseio.com",
        projectId: "angular-many",
        storageBucket: "angular-many.appspot.com",
        messagingSenderId: "769719643626",
    },
};
