export const environment = {
    production: false,
    hmr: false,
    apiURL: "api/",
    apiHost: "https://api.somedomain.com/qa/v1/",
    codes: [ "AB", "AC", "XYZ" ],
};
