export const environment = {
    production: true,
    hmr: false,
    apiURL: "productionApi",
};
