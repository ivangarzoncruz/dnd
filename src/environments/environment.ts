// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    hmr: false,
    apiURL: "api/",
    apiHost: "https://api.somedomain.com/dev/v1/",
    codes: [ "AB", "AC", "XYZ" ],
    firebaseConfig : {
        apiKey: "AIzaSyDS6yMV_cVvAZsD311PE7Z2U4xNQxaKa3Q",
        authDomain: "angular-many.firebaseapp.com",
        databaseURL: "https://angular-many.firebaseio.com",
        projectId: "angular-many",
        storageBucket: "angular-many.appspot.com",
        messagingSenderId: "769719643626",
    },
};
