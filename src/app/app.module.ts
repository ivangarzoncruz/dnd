// Core libraries
import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader } from "@angular/core";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

// Modules
import { CoreModule } from "@core/core.module"
import { SharedModule } from "@shared/shared.module";

// Angular Routing
import { RoutingModule } from "./app.routes";

// PARTIES MODULES
import { SlimLoadingBarModule } from "ng2-slim-loading-bar";

// Components
import { AppComponent } from "./app.component";
import { LayoutComponent } from "./../layouts/layout.component";

// @Ngrx
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { reducers } from "./../store/app.reducer";

// SERVICES
import { AuthService } from "@shared/services/auth/auth.service";
import { AuthGuard } from "@shared/services/auth/auth-guard.service";
import { LoaderInterceptor } from "@shared/services/loader.service";

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
    ],
    imports: [
        BrowserModule,
        CoreModule,
        RoutingModule,
        BrowserAnimationsModule,
        SharedModule.forRoot(),
        SlimLoadingBarModule.forRoot(),
        StoreModule.forRoot( reducers ),
        StoreDevtoolsModule.instrument(),
    ],
    providers: [
        AuthService,
        AuthGuard,
        {
            provide    : NgModuleFactoryLoader,
            useClass   : SystemJsNgModuleLoader,
        },
        {
            provide    : HTTP_INTERCEPTORS,
            useClass   : LoaderInterceptor,
            multi      : true,
        },
    ],
    bootstrap:    [ AppComponent ],
})
export class AppModule { }
