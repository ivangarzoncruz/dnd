import { loadNgModule, Ng2StateDeclaration, Transition } from "@uirouter/angular";

/* Components */
import { AppComponent } from "./app.component";
import { HomeComponent } from "./../pages/home/home.component";
import { UsersComponent } from "./../pages/users/users.component";
import { ThoughtsComponent } from "./../pages/thoughts/thoughts.component";
import { LayoutComponent } from "./../layouts/layout.component";

/** The top level state(s) */
export let APP_STATES: Ng2StateDeclaration[] = [
    {
        component   : LayoutComponent,
        name        : "app",
        data        : { title : "Full Views" },
        abstract    : true,
        url         : "",
    },
    {
        component   : HomeComponent,
        parent      : "app",
        name        : "home",
        url         : "/home",
        // loadChildren: "./../pages/home/home.module#HomeModule",
    },
    {
        component   : UsersComponent,
        parent      : "app",
        name        : "users",
        url         : "/users",
    },
    {
        component   : ThoughtsComponent,
        parent      : "app",
        name        : "thoughts",
        url         : "/thoughts",
    },
];
