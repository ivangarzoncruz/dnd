import { Inject, Injectable } from "@angular/core";
import { Category, trace, UIRouter } from "@uirouter/angular";
import { Visualizer } from "@uirouter/visualizer";

// import { requiresAuthHook } from "./app.hooks";

/*
 * Create your own configuration class (if necessary) for any root/feature/lazy module.
 * Pass it to the UIRouterModule.forRoot/forChild factory methods as `configClass`.
 * The class will be added to the Injector and instantiate when the module loads.
*/
export function RouterConfig( router: UIRouter ): void {

    /* - Enables tracing (check the console) of:
    // - TRANSITION transition start, redirect, success, error, ignored
    // - VIEWCONFIG ui-view component creation/destruction and viewconfig de/activation
    */
    router.trace.enable( Category.TRANSITION, Category.VIEWCONFIG );
    router.plugin( Visualizer );

    // requiresAuthHook( MyUIRouter.transitionService );
    // states.forEach( state => router.stateRegistry.register( state ));

    // Register providers for resolve function parameters
    // let rootState = router.stateRegistry.root();

    // If no URL matches, go to the `home` state by default
    router.urlService.rules.otherwise({ state: "home", params: {} });
}
