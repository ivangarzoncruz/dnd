import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules, ExtraOptions } from "@angular/router";

import { AuthGuard } from "./../shared/services/auth/auth-guard.service";

// IMPORT COMPONENTS
import { LayoutComponent } from "./../layouts/layout.component";
import { NotFoundComponent } from "./../pages/not-found/not-found.component";

import { FULL_ROUTES, LAZY_ROUTES } from "./../pages/pages.routes";

export let APP_ROUTES: Routes = [
    {
        path: "",
        redirectTo: "courses",
        pathMatch: "full",
    },
    {
        path: "",
        component: LayoutComponent,
        data: {
          title: "Full Views",
        },
        children: [
            ...FULL_ROUTES,
            ...LAZY_ROUTES,
        ],
        canActivate: [ AuthGuard ],
    },
    {
        path: "**",
        component: NotFoundComponent,
    },
];

const ROUTES_CONFIG: ExtraOptions = {
    preloadingStrategy: PreloadAllModules,
    useHash: true,
    enableTracing: false,
};

export const RoutingModule: ModuleWithProviders = RouterModule.forRoot( APP_ROUTES, ROUTES_CONFIG );
