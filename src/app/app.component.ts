import { Component } from "@angular/core";

@Component({
    moduleId    : module.id,
    selector    : "app-root",
    styleUrls   : [ "./app.component.scss" ],
    template    : `<span appHighlight>Google</span><router-outlet></router-outlet>`,
})
export class AppComponent { }
