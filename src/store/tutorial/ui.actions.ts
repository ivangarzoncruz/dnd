import { Action } from "@ngrx/store";
import { ITutorial as Tutorial } from "./tutorial.state";

export const UIActionsTypes = {
    TEXTFIELD_CHANGED_ACTION         : "[UI] -Textfield value changed-",
    TAB_CHANGED_ACTION               : "[UI] -Tab changed-",
    SELECT_CHANGED_ACTION            : "[UI] -Select changed-",
    CHECKBOX_VALUE_CHANGED_ACTION    : "[UI] -Check box value changed-",
    RADIO_CHANGED_ACTION             : "[UI] -Radio changed-",
    ADD_TUTORIAL_ACTION              : "[TUTORIAL] Add Tutorial",
    ADD_TUTORIAL_SUCCESS             : "[TUTORIAL] Add Tutorial Success",
    ADD_TUTORIAL_FAIL                : "[TUTORIAL] Add Tutorial Fail",
};

export class TextFieldChangedAction implements Action {
    type = UIActionsTypes.TEXTFIELD_CHANGED_ACTION;
    constructor(public payload: any) { }
}

export class TabChangedAction implements Action {
    type = UIActionsTypes.TAB_CHANGED_ACTION;
    constructor(public payload: any) { }
}

export class SelectChangedAction implements Action {
    type = UIActionsTypes.SELECT_CHANGED_ACTION;
    constructor(public payload: any) { }
}

export class CheckBoxValueChangedAction implements Action {
    type = UIActionsTypes.CHECKBOX_VALUE_CHANGED_ACTION;
    constructor(public payload: any) { }
}

export class RadioChangedAction implements Action {
    type = UIActionsTypes.RADIO_CHANGED_ACTION;
    constructor(public payload: any) { }
}

// /**
//  * Add Tutorial from Tutorial Actions
//  */
export class AddTutorialAction implements Action {
    readonly type = UIActionsTypes.ADD_TUTORIAL_ACTION;
    constructor(public payload: any) {
        // console.log( ' --------- ', payload );
     }
}

export type UIActions =
    | TextFieldChangedAction
    | TabChangedAction
    | SelectChangedAction
    | CheckBoxValueChangedAction
    | RadioChangedAction
    | AddTutorialAction;
