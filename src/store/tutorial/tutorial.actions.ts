// // Section 1
// import { Action } from "@ngrx/store";
// import { ITutorial } from "./tutorial.state";

// // Section 2
// export const TutorialActionsTypes = {
//     ADD_TUTORIAL             : "[TUTORIAL] Add Tutorial",
//     ADD_TUTORIAL_SUCCESS     : "[TUTORIAL] Add Tutorial Success",
//     ADD_TUTORIAL_FAIL        : "[TUTORIAL] Add Tutorial Fail",
//     REMOVE_TUTORIAL          : "[TUTORIAL] Remove Tutorial",
//     REMOVE_TUTORIAL_SUCCESS  : "[TUTORIAL] Remove Tutorial Success",
//     REMOVE_TUTORIAL_FAIL     : "[TUTORIAL] Remove Tutorial Fail",
//     LOAD_TUTORIAL            : "[TUTORIAL] Load Tutorial",
//     LOAD_TUTORIAL_SUCCESS    : "[TUTORIAL] Load Tutorial Success",
//     LOAD_TUTORIAL_FAIL       : "[TUTORIAL] Load Tutorial Fail",
// };

// // Section 3

// /**
//  * Add Tutorail from Tutorial Actions
//  */
// export class AddTutorialAction implements Action {
//     readonly type = TutorialActionsTypes.ADD_TUTORIAL;
//     constructor(public payload: ITutorial) { }
// }

// export class AddTutorialSuccessAction implements Action {
//     readonly type = TutorialActionsTypes.ADD_TUTORIAL_SUCCESS;
//     constructor(public payload: ITutorial) { }
// }

// export class AddTutorialFailAction implements Action {
//     readonly type = TutorialActionsTypes.ADD_TUTORIAL_FAIL;
//     constructor(public payload: ITutorial) { }
// }

// /**
//  * Remove Tutorail from Tutorial Actions
//  */
// export class RemoveTutorialAction implements Action {
//     readonly type = TutorialActionsTypes.REMOVE_TUTORIAL;
//     constructor(public payload: number) {}
// }

// export class RemoveTutorialSuccessAction implements Action {
//     readonly type = TutorialActionsTypes.REMOVE_TUTORIAL_SUCCESS;
//     constructor(public payload: number) {}
// }

// export class RemoveTutorialFailAction implements Action {
//     readonly type = TutorialActionsTypes.REMOVE_TUTORIAL_FAIL;
//     constructor(public payload: number) {}
// }

// /**
//  * Remove Tutorail from Tutorial Actions
//  */
// export class LoadTutorialAction implements Action {
//     readonly type = TutorialActionsTypes.LOAD_TUTORIAL;
//     constructor(public payload: number) {}
// }

// export class LoadTutorialSuccessAction implements Action {
//     readonly type = TutorialActionsTypes.LOAD_TUTORIAL_SUCCESS;
//     constructor(public payload: ITutorial) {}
// }

// export class LoadTutorialFailAction implements Action {
//     readonly type = TutorialActionsTypes.LOAD_TUTORIAL_FAIL;
//     constructor(public payload: any) {}
// }

// // Section 4
// export type TutorialActions =
//     | AddTutorialAction
//     | AddTutorialSuccessAction
//     | AddTutorialFailAction
//     | RemoveTutorialAction
//     | RemoveTutorialSuccessAction
//     | RemoveTutorialFailAction
//     | LoadTutorialAction
//     | LoadTutorialSuccessAction
//     | LoadTutorialFailAction;
