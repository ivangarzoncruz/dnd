export interface UIState {
    textfieldValue: string;
    tutorialsValues: Array<any>;
    selectedTab: string;
    selectValue: string;
    checkBoxValues: Array<any>;
    radioValue: string;
}
