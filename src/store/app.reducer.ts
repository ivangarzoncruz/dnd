import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "./app.state";
import { uiReducer } from "./tutorial/ui.reducer";
// import { tutorialReducer } from "./tutorial/tutorial.reducer";

export const reducers: ActionReducerMap<AppState> = {
    ui: uiReducer,
};

// tutorial: tutorialReducer,
