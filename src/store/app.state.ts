import { UIState } from "./tutorial/ui.state";
// import { ITutorial as Tutorial } from "./tutorial/tutorial.state";

export interface AppState {
    ui: UIState;
    // tutorial: Tutorial[];
}
