import { Component, OnInit, Input } from "@angular/core";

import { IHeroThumbnail as HeroThumbnail } from "././../../../shared/models/marvel.model";

@Component({
  selector: "hero-thumbnail",
  templateUrl: "./thumbnail.component.html",
  styleUrls: ["./thumbnail.component.css"],
})
export class ThumbnailComponent implements OnInit {
    @Input()
    private image: HeroThumbnail;

    @Input()
    private alt: string;

    constructor() {
        //
    }

    public ngOnInit(): void {
        //
    }

    private getThumbnailUrl() {
        return `${this.getThumbnailPathWithoutProtocol()}.${this.image.extension}`;
    }

    private getThumbnailPathWithoutProtocol() {
        return this.image.path.replace(/^https?:/, "");
    }
}
