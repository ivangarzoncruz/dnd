import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HeroesService } from "./heroes.service";

import { HeroesComponent } from "./heroes.component";
import { HeroComponent } from "./hero/hero.component";
import { ThumbnailComponent } from "./thumbnail/thumbnail.component";
import { HeroDetailComponent } from "./hero-detail/hero-detail.component";

import { RoutingModule } from "./heroes.routes";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RoutingModule,
    ],
    declarations: [
        HeroesComponent,
        HeroComponent,
        HeroDetailComponent,
        ThumbnailComponent,
    ],
    providers: [
        HeroesService,
    ],
})
export class HeroesModule { }
