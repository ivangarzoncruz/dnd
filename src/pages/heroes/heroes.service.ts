import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { RequestOptions, Request, RequestMethod } from "@angular/http";

import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { Md5 } from "ts-md5/dist/md5";
// import { ResponseModel } from "./../common/response.model";

import { IHero as Hero } from "././../../shared/models/hero.model";
import { IMarvel as Marvel } from "././../../shared/models/marvel.model";

export interface IPaginationOptions {
    page: number;
    perPage: number;
    query: string;
}

const headerss = new HttpHeaders({
    "Access-Control-Allow-Origin"     : "*",
    "Access-Control-Allow-Methods"    : "GET, POST, PUT, DELETE, PATCH",
    "Access-Control-Allow-Headers"    : "Origin, X-Requested-With, Content-Type, Accept, Authorization",
    "Authorization"                   : "Bearer AKA82120611069",
    "Content-Type"                    : "application/json",
    "Accept"                          : "q=0.8;application/json;q=0.9",
});

const httpOptions = {
    headers: new HttpHeaders({
        "Access-Control-Allow-Origin"     : "*",
        "Access-Control-Allow-Methods"    : "GET, POST, PUT, DELETE, PATCH",
        "Access-Control-Allow-Headers"    : "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        "Content-Type"                    : "application/json",
        "Accept"                          : "q=0.8;application/json;q=0.9",
        "Authorization"                   : "Bearer AKA82120611069",
    }),
};

@Injectable()
export class HeroesService {

    private url: string;
    private api: string;
    private marvel: Marvel[];
    private heroes: Hero[];

    private endpoint: string;
    private public: string;
    private private: string;
    private ts: number;

    private headers: HttpHeaders;

    constructor( @Inject(HttpClient) private http: HttpClient ) {

        this.url = "./assets/data/heroes.json";
        this.api = "/api/heroes";

        this.endpoint = "https://gateway.marvel.com:443/v1/public/characters";
        this.public = "11a99e8af4e8e1a6f4665e3f9d6893a7";
        this.private = "ad7a00b980449a3badd592b588b8d608c8f7e195";
    }

    public getHeroes( options: IPaginationOptions  ): Observable<Marvel[]> {

        const params: HttpParams = this.getListSearchParams( options );

        return this.http
            // .get( this.endpoint, { params : params, headers: new HttpHeaders({ "key'" : "ivancho" }) } )
            .get( this.endpoint, { params } )
            .map( this.onGetHeroesSuccess )
            .catch( this.onGetHeroesError );
    }

    public getHeroById(id: number | string): Observable<Hero[]> {

        return null;

        // return this.http
        //     .get( this.url )
        //     // .map((response) => response)
        //     .map((response: any) => response.filter((item: Hero) => item.id === id))
        //     .catch( this.onGetHeroesError );

        // return this.getHeroes( options: IPaginationOptions )
        //     .map(
        //         (response: any) => {
        //             return response.find((hero: any) => hero.id === id);
        //     })
        //     .catch( this.onGetHeroesError );
    }

    private getBaseParams() {

        const ts: string = this.getTimeStamp();
        const hash: string = this.getHash( ts );
        // let hash = crypto.createHash('md5').update(ts + PRIV_KEY + API_KEY).digest('hex');

        let params = new HttpParams();

        // TODO: Add API key globally for all requests to Marvel Entities API
        params = params.append( "apikey",  this.public );
        params = params.append( "ts", ts );
        params = params.append( "hash", hash );
        // params = params.append( "limit", "20" );

        return params;
    }

    private getListSearchParams( options: IPaginationOptions ): HttpParams {

        let params: HttpParams = this.getBaseParams();
        params = params.append( "limit", String( options.perPage ) );
        params = params.append( "offset", String( options.perPage * ( options.page - 1 ) ));

        // TODO: Add Search parameter
        // if ( this.searchParamName && options.query ) {
        //     params = params.append( this.searchParamName, options.query );
        // }

        return params;
    }

    private getHash( ts: string ): string {
        const hashGenerator: Md5 = new Md5();
        hashGenerator.appendStr( ts );
        hashGenerator.appendStr( this.private );
        hashGenerator.appendStr( this.public );

        const hash: string = hashGenerator.end().toString();
        return hash;
    }

    private getTimeStamp(): string {
        return new Date().valueOf().toString();
    }

    private onGetHeroesSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetHeroesError( error: Response | any ) {
        const message: string = ( error.message ) ? error.message : error.status ? `${error.status} - ${error.statusText}` : "Server error";
        return Observable.throw( message );
    }
}
