import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// IMPORT COMPONENTS
import { HeroesComponent } from "./heroes.component";
import { HeroComponent } from "./hero/hero.component";
import { HeroDetailComponent } from "./hero-detail/hero-detail.component";

export const HEROES_ROUTES: Routes = [
    {
        path: "",
        component: HeroesComponent,
    },
    {
        path: ":id",
        component: HeroDetailComponent,
        // children: [
        //     {
        //         path: ":id/edit",
        //         component: HeroEditComponent,
        //     },
        // ],
    },
];
// { path: "", redirectTo: "/characters", pathMatch: "full" },
// {
//     path: "heroes",
//     component: HeroesComponent,
// },
// {
//     path: "heroes/:id",
//     component: HeroDetailComponent,
// },
// {
//     path: "",
//     redirectTo: "/heroes",
//     pathMatch: "full",
// },
// { path: "comics", component: ComicsListComponent },
// { path: "comics/:id", component: ComicDetailsComponent },
// { path: "creators", component: CreatorsListComponent },
// { path: "creators/:id", component: CreatorDetailsComponent },
// { path: "events", component: EventsListComponent },
// { path: "events/:id", component: EventDetailsComponent },
// { path: "series", component: SeriesListComponent },
// { path: "series/:id", component: SeriesDetailsComponent }

export const RoutingModule: ModuleWithProviders = RouterModule.forChild( HEROES_ROUTES );
