import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    moduleId    : module.id,
    selector    : "app-hero",
    templateUrl : "./hero.component.html",
    styleUrls   : [ "./hero.component.scss" ],
})
export class HeroComponent implements OnInit {
    @Input() private hero: any;

    constructor(
        // private heroesService: HeroesService,
        private router: Router,
        private route: ActivatedRoute,
    ) {
        //
    }

    public ngOnInit(): void {
        //
    }

    private onHeroDetail(): void {
        // console.log( 'Hero ----> ', this.hero );
        this.router.navigate([ "/heroes", this.hero.id ], { relativeTo: this.route });
    }
}
