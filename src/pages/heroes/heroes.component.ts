import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { HeroesService } from "./heroes.service";
import { IMarvel as Marvel } from "././../../shared/models/marvel.model";

@Component({
    moduleId    : module.id,
    selector    : "app-heroes",
    templateUrl : "./heroes.component.html",
    styleUrls   : [ "./heroes.component.scss" ],
})
export class HeroesComponent implements OnInit {

    private marvel: Marvel[];
    private subscription: any;

    private loading: boolean;
    private failed: boolean;

    private selectedHero: Marvel;

    private title: string;
    private query: string;
    private page: number;
    private perPage: number;
    private totalPages: number;

    private alphabet: string;
    private index: object;

    constructor(
        private heroesService: HeroesService,
        private router: Router,
        private route: ActivatedRoute,
    ) {
        this.title = "Tour of Heroes";
        this.query = "";
        this.page = 1;
        this.perPage = 20;
        this.totalPages = 0;

        this.loading = false;
        this.failed = false;

        this.alphabet = "";
        this.index = [];
    }

    public ngOnInit(): void {

        // this.route.params
        //     .subscribe(
        //         (params) => {
        //             this.page = params.p ? parseInt( params.p, 10 ) : 1;
        //             this.query = params.q || "";
        //     });

        this.getHeroes();
        this.printIndex();
    }

    public getHeroes(): void {
        this.subscription = this.heroesService
            .getHeroes({
                page     : this.page,
                perPage  : this.perPage,
                query    : this.query,
            })
            .subscribe( ( marvel ) => {
                this.marvel = marvel;

                // initialize to page 1
                // this.totalPages = this.getTotalPages();
                // this.totalPages = this.marvel.data;
            });

        // Display the loader component
        this.loading = true;
    }

    public onSelect( marvel: Marvel ): void {
        this.selectedHero = marvel;
    }

    private printIndex(): void {

        const first = "a";
        const last = "z";

        for ( let i = first.charCodeAt(0); i <= last.charCodeAt(0); i++ ) {
            this.alphabet += String.fromCharCode(i).toUpperCase();
        }

        // this.alphabet = "abcdefghijklmnopqrstuvwxyz";
        this.index = this.alphabet.split("");
    }

    // private getTotalPages(): number {

    //     if ( this.marvel ) {
    //         return Math.ceil( this.marvel.data.total / this.perPage );
    //         //  =  Math.ceil( this.marvel.data.total / this.perPage );
    //     }
    // }

    private onChangePage( page: number ) {

        // this.router.navigate([{
        //     p: page,
        //     q: this.query,
        // }]);

        this.page = page;
        this.getHeroes();
    }

    private navigate( path: any ) {
        this.router.navigate([{ outlets: { primary : path, sidemenu : path }}], { relativeTo : this.route });
    }

    private ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
