import { Component, OnInit, OnDestroy, Input, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import "rxjs/add/operator/takeWhile";
// import { Transition } from "@uirouter/angular";

import { HeroesService } from "./../heroes.service";
import { IHero as Hero } from "./../../../shared/models/hero.model";

@Component({
    moduleId    : module.id,
    selector    : "app-hero-detail",
    templateUrl : "./hero-detail.component.html",
    // styleUrls: [ "./hero.scss" ],
})
export class HeroDetailComponent implements OnInit {

    @Input()
    private id: number;
    private hero: Hero[];
    private subscription: any;
    private sub: any;

    constructor(
        private heroesService: HeroesService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
    ) { }

    public ngOnInit(): void {

        // Subscribe to route params
        this.subscription = this.route.params
            .subscribe(
                (params) => {
                   this.id = params.id; // @Get the id from the route
                   // this.getHeroById( Number( this.id ) );
            });
    }

    private getHeroById(id: number): void {
        this.heroesService
            .getHeroById( id )
            .subscribe( (hero) => this.hero = hero );
    }

    private ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    private onBack(): void {
        this.router.navigate([ "../heroes" ]);
    }
}
