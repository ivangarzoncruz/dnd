import { OrderDetailComponent } from "./order-detail/order-detail.component";
import { OrderTestComponent } from "./order-test/order-test.component";

export const components: any[] = [
    OrderDetailComponent,
    OrderTestComponent,
];

export * from "./order-detail/order-detail.component";
export * from "./order-test/order-test.component";
