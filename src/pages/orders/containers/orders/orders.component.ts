import { Component, OnInit, Input } from "@angular/core";

import { OrdersService } from "./../../services/orders.service";
import { map, switchMap, catchError } from "rxjs/operators";

export interface IPet {
    name: string;
    age: string;
}

@Component({
    moduleId    : module.id,
    selector    : "app-orders",
    templateUrl : "./orders.component.html",
})
export class OrdersComponent {

    private subscription: any;
    private pets: any;

    constructor( private ordersService: OrdersService ) {}

    ngOnInit(): void {
        // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        // Add 'implements OnInit' to the class.

        // this.ordersService.getData().snapshotChanges()
        //     .pipe(
        //         map( ( actions ) =>
        //             actions.map( (a) => (
        //                { key: a.key, ...a.payload.val() }
        //             )),
        //         ),
        //     ).subscribe( (items) => {
        //         this.pets = items;
        //       });

        this.subscription = this.ordersService.getData()
            .valueChanges()
            .subscribe( ( pets ) => {
                this.pets = pets;
            });
    }

    onSubmit() {
        this.ordersService.insertPet();
    }
}
