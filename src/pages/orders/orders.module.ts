import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

/** IMPORT MODULES */
import { SharedModule } from "@shared/shared.module";

// Containers
import * as fromContainers from "./containers";

// Components
import * as fromComponents from "./components";

// IMPORT ROUTES
import { RoutingModule } from "./orders.routes";

import { OrdersService } from "./services/orders.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RoutingModule,
        SharedModule,
    ],
    declarations: [
        ...fromComponents.components,
        ...fromContainers.containers,
    ],
    providers: [
        OrdersService,
    ],
})
export class OrdersModule { }
