import { Injectable } from "@angular/core";

import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { Observable } from "rxjs/Observable";

export interface Pet {
    name: string;
    age: string;
}

@Injectable()
export class OrdersService {

    public petList: AngularFireList<Pet>;
    // selectedEmployee: Pet = new Pet();

    constructor( protected firebase: AngularFireDatabase ) { }

    getData() {
        return this.firebase.list<Pet>( "pets" );
    }

    insertPet() {
        return this.firebase.list( "pets" )
                .push({ name: "Lisa", age: "3 years" });
    }

    // updateEmployee( employee: Employee){
    //     this.employeeList
    //         .update( employee.$key,  {
    //             name: employee.name,
    //             position: employee.position,
    //             office: employee.office,
    //             salary: employee.salary
    //         });
    // }

    // deleteEmployee( $key: string ): void {
    //     this.employeeList.remove( $key );
    // }
}
