import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Containers
import * as fromContainers from "./containers";

// Components
import * as fromComponents from "./components";

const ORDERS_ROUTES: Routes = [
    {
        path        : "",
        component   : fromContainers.OrdersComponent,
    },
    {
        path        : "new",
        component   : fromComponents.OrderTestComponent,
    },
    {
        path        : ":id",
        component   : fromComponents.OrderDetailComponent,
    },
];

export const RoutingModule: ModuleWithProviders = RouterModule.forChild( ORDERS_ROUTES );
