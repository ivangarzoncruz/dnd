import { Routes, RouterModule } from "@angular/router";

// IMPORT COMPONENTS
import { HomeComponent } from "./home/home.component";
import { ChatComponent } from "./chat/chat.component";
import { UsersComponent } from "./users/containers/users/users.component";
import { ThoughtsComponent } from "./thoughts/thoughts.component";
import { MoviesComponent } from "./movies/movies.component";
import { WorldcupComponent } from "./worldcup/worldcup.component";
import { TUTORIAL_ROUTES } from "./tutorial/tutorial.routes";

export let FULL_ROUTES: Routes = [
    {
        path        : "home",
        data        : { title : "Home" },
        component   : HomeComponent,
    },
    {
        path        : "chat",
        data        : { title : "Chat" },
        component   : ChatComponent,
    },
    {
        path        : "users",
        data        : { title : "Users" },
        component   : UsersComponent,
    },
    {
        path        : "thoughts",
        data        : { title : "Thoughts" },
        component   : ThoughtsComponent,
    },
    {
        path        : "movies",
        data        : { title : "Movies" },
        component   : MoviesComponent,
    },
    {
        path        : "worldcup",
        data        : { title : "Worldcup" },
        component   : WorldcupComponent,
    },
];

export let LAZY_ROUTES: Routes = [
    {
        path            : "jokes",
        data            : { title : "Jokes" },
        loadChildren    : "./../pages/jokes/jokes.module#JokesModule",
    },
    {
        path            : "orders",
        data            : { title : "Orders" },
        loadChildren    : "./../pages/orders/orders.module#OrdersModule",
    },
    {
        path            : "heroes",
        data            : { title : "Heroes" },
        loadChildren    : "./../pages/heroes/heroes.module#HeroesModule",
    },
    {
        path            : "pets",
        data            : { title : "Pets" },
        loadChildren    : "./../pages/pets/pets.module#PetsModule",
    },
    {
        path            : "weather",
        data            : { title : "Weather" },
        loadChildren    : "./../pages/weather/weather.module#WeatherModule",
    },
    {
        path            : "courses",
        data            : { title : "Courses" },
        loadChildren    : "./../pages/courses/courses.module#CoursesModule",
    },
    ...TUTORIAL_ROUTES,
];

// Lazy Loading the Home module
// We will now refactor this code in order to make the Home component and anything inside
// HomeModule to be lazy loaded. This means everything related to the HomeModule will only
// be loaded from the server if we hit the home link (see the App HTML template above) in
// the App component, but not on initial load.

// The first thing that we need to do is to remove every mention of the Home component or
// the HomeModule from the App component and the main routing configuration:

// We can then import these constants into the module definition,
// using the array spread ... operator
