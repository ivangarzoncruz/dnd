import { Http } from "@angular/http";
import { Ng2StateDeclaration, Transition } from "@uirouter/angular";

/* Components */
import { JokesComponent } from "./jokes.component";
import { JokeComponent } from "./joke.component";

// export function getJokes( JokesService ) {
//   return JokesService.getJokes();
// }

/**
 * This file defines the states for the `jokes` module.
 * The states are exported as an array and imported in the JokesModule.
 */
export let JOKES_STATES: Ng2StateDeclaration[] = [

  // A state for the 'app.jokes' submodule.
  // - Fills in the unnamed <ui-view> from `app` state with `JokesComponent`
  // - Fetches jokes data using a resolve, then the component displays the data
  {
    name: "app.jokes",
    url: "/jokes",
    component: JokesComponent
    // resolve: [
    //     {
    //         token: "jokes",
    //         policy: { when: "EAGER" },
    //         deps: [ JokesService ],
    //         resolveFn: getJokes
    //     }
    // ]
  }

  // A child state of app.jokes
  // - This state fills the unnamed <ui-view> (in the `JokesComponent` from  `app.jokes` state) with
  // - Has a path parameter :jokeId which appears in the URL
  // - Resolves JokeComponent, then the component displays the data
  // {
  //   name: "jokes.joke",
  //   url: "/:jokeId",
  //   component: JokeComponent
  // }
];