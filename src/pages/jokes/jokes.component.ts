import { Component, OnInit, Input } from "@angular/core";
import { IJoke as Joke } from "./joke.model";

const JOKES: Joke[] = [
  { title: "What did the cheese say when it looked in the mirror?", author: "Hello-me (Halloumi)", hide: true },
  { title: "What kind of cheese do you use to disguise a small horse?", author: "Mask-a-pony (Mascarpone)", hide: true },
  { title: "A kid threw a lump of cheddar at me", author: "Mr. Nice", hide: true },
];

@Component({
    moduleId    : module.id,
    selector    : "app-jokes",
    templateUrl : "./jokes.component.html",
    // styleUrls   : [ "./jokes.component.scss" ],
})
export class JokesComponent implements OnInit {

    private title: string = "Tour of Jokes";
    private jokes: Joke[] = JOKES;
    private selectedJoke: Joke;

    public ngOnInit() {
        // Init
    }
}
