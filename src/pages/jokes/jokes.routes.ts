import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// IMPORT COMPONENTS
import { JokesComponent } from "./jokes.component";

const JOKES_ROUTES: Routes = [
    {
        path       : "",
        component  : JokesComponent,
    },
];

export const JokesRoutingModule: ModuleWithProviders = RouterModule.forChild( JOKES_ROUTES );
