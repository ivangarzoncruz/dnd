import { Component, OnInit, Input } from "@angular/core";
import { IJoke as Joke } from "./joke.model";

@Component({
  selector: "joke-detail",
  template: `
    <div *ngIf="joke">
      <div class="card card-block">
        <h4 class="card-title">{{joke.title}}</h4>
        <p class="card-text" [hidden]="joke.hide">{{joke.author}}</p>
        <a (click)="toggle()" class="btn btn-warning">Tell Me</a>
      </div>
    </div>
  `
})
export class JokeComponent {
  @Input()
  private joke: Joke;

  public toggle(): void {
    this.joke.hide = !this.joke.hide;
  }
}