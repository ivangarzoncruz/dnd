import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// IMPORT COMPONENTS
import { JokesComponent } from "./jokes.component";
import { JokeComponent } from "./joke.component";

// IMPORT ROUTES
import { JokesRoutingModule } from "./jokes.routes";

@NgModule({
    imports: [
        CommonModule,
        JokesRoutingModule,
    ],
    declarations: [
        JokesComponent,
        JokeComponent,
    ],
    providers: [],
})
export class JokesModule { }
