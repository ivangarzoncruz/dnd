export interface IJoke {
  title: string;
  author: string;
  hide: boolean;
}