import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { WorldcupService } from "./worldcup.service";
import { WorldcupComponent } from "./worldcup.component";

import { GroupByPipe } from "././../../shared/pipes/groupby.pipe";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        WorldcupComponent,
        GroupByPipe,
    ],
    providers: [
        WorldcupService,
    ],
})
export class WorldcupModule { }
