import { Component, OnInit } from "@angular/core";

import { ITeams as Teams } from "./../../shared/models/teams.model";
import { WorldcupService } from "./worldcup.service";

@Component({
    moduleId    : module.id,
    selector    : "app-worldcup",
    templateUrl : "./worldcup.component.html",
    styleUrls   : [ "./worldcup.component.scss" ],
})
export class WorldcupComponent implements OnInit {

    private teams: Teams[];
    private subscription: any;

    constructor( public worldcupService: WorldcupService ) { }

    public ngOnInit() {
        this.getTeams();
    }

    private getTeams(): void {
        this.subscription = this.worldcupService
            .getTeams()
            .subscribe( ( teams ) => {
                this.teams = teams;

                // const groupByName = {};

                // this.teams.forEach( function (a) {
                //     groupByName [ a.group ] = groupByName [ a.group ] || [];
                //     groupByName [ a.group ].push( { date: a.date, is_present: a.is_present });
                // });
            });
      }

}
