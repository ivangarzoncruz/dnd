import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { RequestOptions, Request, RequestMethod } from "@angular/http";

import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { ITeams as Teams } from "./../../shared/models/teams.model";

@Injectable()
export class WorldcupService {

    private url: string;
    private teams: Teams[];

    constructor( @Inject(HttpClient) private http: HttpClient ) {
        this.url = "https://worldcup.sfg.io/teams/";
    }

    public getTeams(): Observable<Teams[]> {
        return this.http
            .get( this.url )
            .map( this.onGetTeamsSuccess )
            .do( (teams) => this.teams = teams )
            .catch( this.onGetTeamsError );
    }

    private onGetTeamsSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetTeamsError( error: any ): Observable<any> {
        const message: string = ( error.message ) ? error.message : error.status ? `${error.status} - ${error.statusText}` : "Server error";
        return Observable.throw( message );
    }

}
