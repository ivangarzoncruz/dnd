/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WorldcupService } from './worldcup.service';

describe('Service: Worldcup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorldcupService]
    });
  });

  it('should ...', inject([WorldcupService], (service: WorldcupService) => {
    expect(service).toBeTruthy();
  }));
});
