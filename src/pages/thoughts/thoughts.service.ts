import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { queryBuilder } from "./util.helpers";
import { IThought as Thought } from "./../../shared/models/thought.model";

const GRAPHQL_API = "http://localhost:8040/api";

@Injectable()
export class ThoughtsService {
    constructor(private http: HttpClient) {}

    public getList() {
        return this.http
            .post( GRAPHQL_API,
                queryBuilder({
                    type: "query",
                    operation: "thoughts",
                    fields: [ "id", "name", "thought", "user { id, firstname, lastname }" ],
                }))
            .map( (res: any) => res.data.thoughts );
    }
}
