import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { ThoughtsService } from "./thoughts.service";
import { ThoughtsComponent } from "./thoughts.component";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        ThoughtsComponent,
    ],
    providers: [
        ThoughtsService,
    ],
})
export class ThoughtsModule { }
