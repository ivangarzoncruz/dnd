import { Component, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

import { IThought as Thought } from "./../../shared/models/thought.model";
import { ThoughtsService } from "./thoughts.service";

@Component({
    moduleId    : module.id,
    selector    : "app-thoughts",
    template    : `
        <h1>Thoughts</h1>
        <ul>
          <li *ngFor="let item of thoughts">
            <span>{{ item.thought }}</span>
            <span>by : {{ item.user.firstname }}</span>
          </li>
        </ul>
    `,
    styles: [],
})
export class ThoughtsComponent implements OnInit {

    public thoughts: Thought[];

    constructor( public thoughtsService: ThoughtsService ) { }

    public ngOnInit() {
        this.getThoughts();
    }

    private getThoughts(): void {
      this.thoughtsService
          .getList()
          .subscribe((thoughts) => this.thoughts = thoughts );
    }
}
