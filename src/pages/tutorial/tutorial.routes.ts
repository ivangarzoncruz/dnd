import { Routes } from "@angular/router";
import { TutorialComponent } from "./tutorial.component";

// Route Configuration
export const TUTORIAL_ROUTES: Routes = [
    {
        path: "tutorial",
        component: TutorialComponent,
    },
    {
        path: "tutorial/:id",
        component: TutorialComponent,
    },
];
