import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

// import { Store } from "@ngrx/store";
// import { AppState } from "../../store/app.state";
// import { TextFieldChangedAction, AddTutorialAction } from "./../../store/tutorial/ui.actions";

// import { ITutorial as Tutorial } from "./../../store/tutorial/tutorial.state";

@Component({
    moduleId    : module.id,
    selector    : "app-tutorial",
    templateUrl : "./tutorial.component.html",
    styleUrls   : ["./tutorial.component.scss"],
})

export class TutorialComponent implements OnInit {

    txtfieldValue: Observable<string>;
    tutorialsValues: any[];

    // constructor(private store: Store<AppState>) {
    constructor() {

        // this.txtfieldValue = this.store.select( (state) => state.ui.textfieldValue );
        // this.store.select( (state) => state.ui.tutorialsValues ).subscribe(x =>
        // {
        //     this.tutorialsValues = x;
        // });

    }

    onChangeModelValue(event: any) {
        // this.store.dispatch(new TextFieldChangedAction(event));
    }

    addTutorial( name: string, url: string ) {

        const value = [
            {
                name: name,
                url: url,
            },
        ];

        // const tutorialsValues = value.concat(this.tutorialsValues);
        // this.store.dispatch( new AddTutorialAction( tutorialsValues ) );
    }

    public ngOnInit(): void {
    }
}
