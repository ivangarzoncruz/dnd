import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
    moduleId    : module.id,
    selector    : "page-not-found",
    templateUrl : "not-found.component.html",
    styleUrls   : [ "./not-found.component.scss" ],
})
export class NotFoundComponent {
    pageTitle: string = "Not found!";

    constructor(private location: Location) {

    }

    private goBack(): void {
        this.location.back();
    }
}
