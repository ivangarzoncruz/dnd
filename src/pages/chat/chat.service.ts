import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import * as io from "socket.io-client";

/** Models */
export interface IUser {
    id?: number;
    name?: string;
    avatar?: string;
}

export interface IMessage {
    from?: IUser;
    content?: any;
    action?: Action;
}

export enum Event {
    CONNECT = "connect",
    DISCONNECT = "disconnect",
}

export enum Action {
    JOINED,
    LEFT,
    RENAME,
}
/** End Models */

@Injectable()
export class ChatService {

    private url = "http://localhost:8095";
    private socket: any;

    constructor() { }

    public sendMessage( message: IMessage ): void {

        console.log( "Message is : ", message );
        this.socket.emit( "message", message );
    }

    public getMessages() {
        return new Observable(( observer: any ) => {
            this.socket = io(this.url);
            this.socket.on( "message", ( data: any ) => {
                observer.next( data );
            });
            return () => {
                this.socket.disconnect();
            };
        });
        // return observable;
    }

    public getUsername(): string {
        return sessionStorage.getItem( "username" );
    }

    public setUsername( username: string ): void {
        sessionStorage.setItem( "username", username );
    }

    // public setUser( user: IUser ): void {
    //     sessionStorage.setItem( "user", JSON.stringify( user ));
    // }

    public onEvent(event: Event): Observable<any> {
        return new Observable<Event>(( observer: any ) => {
            this.socket.on( event, () => {
                observer.next();
            });
            return () => {
                this.socket.disconnect();
            };
        });
    }
}
