import { Component, OnInit, OnDestroy } from "@angular/core";
import { ChatService } from "./chat.service";

/** Models */
export interface IUser {
    id?: number;
    username?: string;
    avatar?: string;
}

export interface IMessage {
    from?: IUser;
    content?: any;
    action?: Action;
}

export enum Event {
    CONNECT = "connect",
    DISCONNECT = "disconnect",
}

export enum Action {
    JOINED,
    LEFT,
    RENAME,
    MESSAGE,
}
/** End Models */

const AVATAR_URL = "https://api.adorable.io/avatars/285";

@Component({
    moduleId    : module.id,
    selector    : "app-chat",
    templateUrl : "./chat.component.html",
    styleUrls   : [ "./chat.component.scss" ],
})
export class ChatComponent implements OnInit {

    action = Action;
    user: IUser;
    messages: IMessage[] = [];
    connection: any;
    content: string;
    username: string;

    constructor( private chatService: ChatService ) { }

    public ngOnInit(): void {

        const randomId = this.getRandomId();
        const name = this.chatService.getUsername();

        this.user = {
            id        : randomId,
            avatar    : `${AVATAR_URL}/${randomId}.png`,
            username  : name,
        };

        this.connection = this.chatService.getMessages()
            .subscribe(
                (message) => {
                    this.messages.push( message );
                },
        );

        this.chatService.onEvent( Event.CONNECT )
            .subscribe(() => {
                console.log( "connected" );
            });

        this.chatService.onEvent( Event.DISCONNECT )
            .subscribe(() => {
                console.log( "disconnected" );
            });
    }

    public ngOnDestroy(): void {
        this.connection.unsubscribe();
    }

    public setUsername(): void {
        this.chatService.setUsername( this.user.username );
    }

    public sendMessage( message: string ): void {
        if ( !message ) {
            return;
        }

        this.chatService.sendMessage({
            from    : this.user,
            content : message,
        });

        this.content = null;
    }

    private getRandomId(): number {
        return Math.floor(Math.random() * (1000000)) + 1;
    }
}
