import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { ChatComponent } from "./chat.component";
import { ChatService } from "./chat.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        ChatComponent,
    ],
    providers: [
        ChatService,
    ],
})
export class ChatModule { }
