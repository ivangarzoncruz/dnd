import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule  } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

/** IMPORT MODULES WITH NOT LAZY LOADING FEATURE */
import { HomeModule } from "./home/home.module";
import { ThoughtsModule } from "./thoughts/thoughts.module";
import { UsersModule } from "./users/users.module";
import { ChatModule } from "./chat/chat.module";
import { MoviesModule } from "./movies/movies.module";
import { WorldcupModule } from "./worldcup/worldcup.module";
import { NotFoundModule } from "./not-found/not-found.module";

// IMPORT COMPONENTS
import { TutorialComponent } from "./tutorial/tutorial.component";

@NgModule({
    imports: [
        CommonModule,
        UsersModule,
        ThoughtsModule,
        HomeModule,
        ChatModule,
        WorldcupModule,
        NotFoundModule,
        MoviesModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        TutorialComponent,
    ],
    providers: [],
})
export class PagesModule { }
