import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { User } from "./../models/user.model";

@Injectable()
export class UsersService {

    private url: string;
    private users: User[];

    private httpOptions = {
        headers: new HttpHeaders({ "Content-Type" : "application/json" }),
    };

    constructor( @Inject(HttpClient) private http: HttpClient ) {
        // this.url = "http://localhost:8030/api/users";
        this.url = "https://randomuser.me/api/?results=10&seed=abc";
    }

    public getUsers(): Observable<User[]> {
        return this.http
            .get( this.url )
            .map( this.onGetUsersSuccess )
            .catch( this.onGetUsersError );
    }

    private onGetUsersSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetUsersError( error: any ): Observable<any> {
        const message: string = ( error.message ) ? error.message : error.status ? `${error.status} - ${error.statusText}` : "Server error";
        return Observable.throw( message );
    }
}
