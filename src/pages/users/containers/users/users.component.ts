import { Component, Input, OnInit } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

import { User } from "./../../models/user.model";
import { UsersService } from "./../../services/users.service";

@Component({
    moduleId    : module.id,
    selector    : "user-list",
    templateUrl : "./users.component.html",
})
export class UsersComponent implements OnInit {

    @Input()
    public users: User[];
    private subscription: any;

    constructor( public usersService: UsersService ) { }

    public ngOnInit(): void {
        this.getUsers();
    }

    public getUsers(): void {
        this.subscription = this.usersService
            .getUsers()
            .subscribe( ( users ) => {
                this.users = users;
                console.log( this.users );
            });
    }
}
