import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";

import { UsersComponent } from "./containers/users/users.component";
import { UsersService } from "./services/users.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        UsersComponent,
    ],
    providers: [
        UsersService,
    ],
})
export class UsersModule { }
