import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// import { ModalService } from "@shared/services/modal.service";

@Component({
    moduleId    : module.id,
    selector    : 'app-course-form',
    templateUrl : './course-form.component.html',
    styleUrls   : [ './course-form.component.scss' ],
})
export class CourseFormComponent implements OnInit {

    public avatar: string;
    public courseForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        // private modalService: ModalService,
    ) { }

    public ngOnInit() {
        this.createForm();
    }

    public createForm(): void {

        this.courseForm = new FormGroup({
            'name': new FormControl( '', [
                Validators.required,
                Validators.minLength(4),
            ]),
            'description': new FormControl( '', [
                Validators.required,
            ]),
        });

        // this.courseForm = this.fb.group({
        //     name: this.fb.control( '', Validators.required ),
        //     description: this.fb.control( '', Validators.required )
        // });
    }

    public onSubmit( data: Course ) {

        if ( this.courseForm.valid ) {
            // this.coursesService.insertCourse( data );
            this.courseForm.reset();
        } else {
            console.log( "Form Not Valid...!!! " );
        }

    }

    public preview( $e: any ): void {
        this.avatar = $e.avatar;
    }

    public upload( $e: any ): void {
        this.avatar = $e.avatar;
    }

}
