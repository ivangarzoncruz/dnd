import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

// export interface Image {
//     id: string;
//     type: string;
//     format: string;
//     size: number;
//     path: string;
//     url: string;
//     name: string;
//     created: number;
// }

@Component({
    moduleId    : module.id,
    selector    : 'app-course-grid-images',
    templateUrl : './course-grid-images.component.html',
    styleUrls   : [ './course-grid-images.component.scss' ],
})
export class CourseGridImagesComponent implements OnInit {

    @Input()
    public images: any[];

    @Output()
    public picker = new EventEmitter<any>();

    @Output()
    public load = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {
    }

    uploadImage() {
        this.picker.emit();
    }

    selectedImage( e: Event, image: any ) {

        console.log( 'image -----> ', image );

        this.load.emit({
            image: image,
        });
    }

    getImageWidth( image: any ) {
        console.log( image );
        console.log( (image.width * 200 / image.height) + 'px' );
        return (image.width * 200 / image.height) + 'px';
    }

    getImageFlexGrow( image: any ) {
        return image.width * 200 / image.height;
    }

    getImagePaddingBottom( image: any ) {
        return (image.height / image.width * 100) + '%';
    }
}
