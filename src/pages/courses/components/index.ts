import { CoursesListComponent } from "./courses-list/courses-list.component";
import { CourseFormComponent } from "./course-form/course-form.component";
import { CourseImageUploadComponent } from "./course-image-upload/course-image-upload.component";
import { CourseGridImagesComponent } from "./course-grid-images/course-grid-images.component";

export const components: any[] = [
    CoursesListComponent,
    CourseFormComponent,
    CourseImageUploadComponent,
    CourseGridImagesComponent,
];

export * from "./courses-list/courses-list.component";
export * from "./course-form/course-form.component";
export * from "./course-image-upload/course-image-upload.component";
export * from "./course-grid-images/course-grid-images.component";
