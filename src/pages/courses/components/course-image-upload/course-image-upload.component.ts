import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { ModalService } from "@shared/services/modal.service"
import { UploadService } from "@shared/services/upload/upload.service";
import { CoursesService } from "./../../services/courses.service";
// import { Upload } from "@shared/models/upload.model"

import { CropperSettings, ImageCropperComponent } from 'ng2-img-cropper';


export interface Image {
    id: string;
    type: string;
    format: string;
    size: number;
    path: string;
    url: string;
    name: string;
    created: number;
}

@Component({
    moduleId    : module.id,
    selector    : 'app-course-image-upload',
    templateUrl : './course-image-upload.component.html',
    styleUrls   : [ './course-image-upload.component.scss' ]
})
export class CourseImageUploadComponent implements OnInit {

    @Output()
    public preview = new EventEmitter<any>();

    @Output()
    public upload = new EventEmitter<any>();

    @Output()
    public modal = new EventEmitter<any>();

    @Input()
    public avatar: any;

    @ViewChild( 'fileInput' )
    public fileInput: ElementRef;

    // public avatar: string;
    public imgName: string;
    public imgType: string;
    public imgFormat: string;
    public imgSize: number;
    public imgPath: string;
    public grid: boolean;

    public selectedFiles: FileList;
    public images: Image[];
    public subscription: any;

    public ref: AngularFireStorageReference;
    public task: AngularFireUploadTask;
    public uploadState: Observable<string>;
    public uploadProgress: Observable<number>;
    public downloadURL: Observable<string>;

    data: any;
    cropperSettings: CropperSettings;

    @ViewChild('cropper', undefined)
    cropper: ImageCropperComponent;

    uploadedImage: File;
    imagePreview: string;

    constructor(
        private uploadService: UploadService,
        private coursesService: CoursesService,
        private modalService: ModalService,
        private af: AngularFireStorage,
        private db: AngularFireDatabase,
        private afs: AngularFirestore,


    ) {

        this.cropperSettings = new CropperSettings();
        this.cropperSettings.croppedWidth = 100;
        this.cropperSettings.croppedHeight = 100;
        this.cropperSettings.canvasWidth = 600;
        this.cropperSettings.canvasHeight = 400;
        this.cropperSettings.compressRatio = 2.70;
        this.cropperSettings.noFileInput = true;
        this.data = {};


    }

    public ngOnInit(): void {
        this.getImages();
    }

    fileChangeListener( event: any ) {


        // this.uploadedImage = new File([result], result.name);
        // this.getImagePreview(this.uploadedImage);

        const image: any = new Image();
        const file: File = event.target.files[0];

        const reader = new FileReader();
            reader.readAsDataURL( file ); // read file as data url
            reader.onload = ( event ) => { // called once readAsDataURL is completed
                image.src = event.target.result;
                this.cropper.setImage( image );
            }

            // this.uploadedImage = new File([result], result.name);
            // this.getImagePreview(this.uploadedImage);

            // myReader.onloadend = function ( loadEvent: any ) {
            //   image.src = loadEvent.target.result;
            //   that.cropper.setImage(image);
            // };

            // reader.readAsDataURL( file );
    }

    public onFileChanged( event: any ): void {

        if ( event.target.files && event.target.files[0] ) {

            const reader = new FileReader();
            reader.readAsDataURL( event.target.files[0] ); // read file as data url

            reader.onload = ( event ) => { // called once readAsDataURL is completed

                // console.log( event.target );

                this.preview.emit({
                    avatar: event.target[ 'result' ],
                });
            }

            // this.selectedFiles = event.target.files;
            // this.uploadFile();
        }
    }

    public uploadFile(): void {

        let file = this.selectedFiles.item( 0 );

        const fileToUpload = {
            name : file.name,
            file : file,
            size : file.size,
        };

        this.uploadService.pushUpload( fileToUpload );
    }

    public getImages(): void {

        this.subscription = this.uploadService.getFileUploads( 10 )
            .valueChanges()
            .subscribe( ( images ) => {
                this.images = images.reverse();
            });
    }

    public picker( e: Event ): void {

        let event: Event = new MouseEvent( 'click' );
        this.fileInput.nativeElement.dispatchEvent( event );

    }

    public load( e: any ): void {

        this.avatar = e.image.url;
        this.grid = !this.grid;

        // let event: Event = new MouseEvent( 'click' );
        // this.fileInput.nativeElement.dispatchEvent( event );
    }

    public openModal( id: string ): void {
        this.modalService.open( id );
    }

    public closeModal( id: string ): void {
        this.modalService.close( id );
    }
}
