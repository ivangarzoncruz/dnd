import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Containers
import * as fromContainers from "./containers";

// Components
import * as fromComponents from "./components";

const COURSES_ROUTES: Routes = [
    {
        path        : "",
        component   : fromContainers.CoursesComponent,
    },
    {
        path        : ":id",
        component   : fromComponents.CoursesListComponent,
    },
];

export const RoutingModule: ModuleWithProviders = RouterModule.forChild( COURSES_ROUTES );
