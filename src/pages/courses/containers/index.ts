import { CoursesComponent } from "./courses/courses.component";

export const containers: any[] = [
    CoursesComponent,
];

export * from "./courses/courses.component";
