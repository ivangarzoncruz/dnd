import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// import { Course } from "./../../models/course.model";
import { CoursesService } from "./../../services/courses.service";
import { UploadService } from "@shared/services/upload/upload.service"
// import { BoldHoverDirective } from './bold-hover.directive';

@Component({
    moduleId    : module.id,
    selector    : 'app-courses',
    templateUrl : './courses.component.html',
    styleUrls   : [ './courses.component.scss' ],

})
export class CoursesComponent implements OnInit {

    public subscription: any;
    public sub: any;
    public courses: Course[];
    public images: any[];

    constructor(
        private coursesService: CoursesService,
        private uploadService: UploadService,
    ) { }

    ngOnInit() {

        this.subscription = this.coursesService.getCourses()
            .valueChanges()
            .subscribe( ( courses ) => {
                this.courses = courses;
            });

        // this.createForm();
    }

    public renderRectangles( event: Event ): void {

        console.log( event )

		console.group( "Text Select Event" );
		console.log( "Event:", event );
		// console.log( "Id:", event.id );
		// console.log( "Text:", event.text );
		// console.log( "Viewport Rectangle:", event.viewportRectangle );
		// console.log( "Host Rectangle:", event.hostRectangle );
        console.groupEnd();
    }
}
