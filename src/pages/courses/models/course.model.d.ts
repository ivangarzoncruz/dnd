interface Course {
    id: string;
    name: string;
    description: string;
    avatar: string;
}
