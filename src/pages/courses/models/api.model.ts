// api.model.ts
    namespace ApiModel {
    export interface Course {
        id: string;
        name: string;
        description: string;
        avatar: string;
    }
}

// using the interfaces
// export class MyComponent {
//     pet: ApiModel.Ipet;
//     query: ApiModel.IQuery;
// }

