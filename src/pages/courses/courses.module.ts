import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

/** IMPORT MODULES */
import { SharedModule } from "@shared/shared.module";

// Containers
import * as fromContainers from "./containers";

// Components
import * as fromComponents from "./components";

// Services
import * as fromServices from "./services";

// IMPORT ROUTES
import { RoutingModule } from "./courses.routes";

import { ImageCropperModule } from 'ng2-img-cropper';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RoutingModule,
        SharedModule,
        ImageCropperModule,
    ],
    declarations: [
        ...fromComponents.components,
        ...fromContainers.containers,
    ],
    providers: [
        ...fromServices.services,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class CoursesModule { }
