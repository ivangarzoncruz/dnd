import { Injectable } from "@angular/core";

import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { Observable } from "rxjs/Observable";

// import { ApiModel } from "./../models/api.model";

@Injectable()
export class CoursesService {

    public course: Course;

    constructor( protected firebase: AngularFireDatabase ) { }

    getCourses() {
        return this.firebase.list<Course>( "courses" );
    }

    insertCourse( course: Course ) {
        return this.firebase.list( "courses" ).push( course );
    }
}

