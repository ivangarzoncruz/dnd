import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MoviesService } from "./movies.service";
import { MoviesComponent } from "./movies.component";

const MOVIES_ROUTES: Routes = [
    {
        path        : "movies",
        data        : { title : "Movies" },
        component   : MoviesComponent,
    },
];

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        MoviesComponent,
    ],
    providers: [
        MoviesService,
    ],
})
export class MoviesModule {}
