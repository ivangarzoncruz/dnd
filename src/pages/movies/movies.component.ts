import { Component, OnInit, OnDestroy } from "@angular/core";
import { MoviesService } from "./movies.service";

@Component({
    moduleId    : module.id,
    selector    : "app-movies",
    styleUrls   : [ "./movies.component.scss" ],
    templateUrl : "./movies.component.html",
    providers   : [ MoviesService ],
})
export class MoviesComponent implements OnInit {

    public movies: object;
    private subscription: any;

    constructor( private moviesService: MoviesService ) { }

    public ngOnInit(): void {
        // this.getMovies();
    }

    public getMovies(): void {
        this.subscription = this.moviesService
            .getMovies()
            .subscribe( (movies) => {
                this.movies = movies;
            });
    }

    private ngOnDestroy(): void {
        // this.subscription.unsubscribe();
    }

}
