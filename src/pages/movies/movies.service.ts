import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Observable } from "rxjs";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
// import { HttpHeaders } from './../common/headers';

import { IMovie as Movie } from "././../../shared/models/movie.model";

@Injectable()
export class MoviesService {

    // private options: RequestOptions;
    private movie: Movie[];
    private url: string;
    private key: string;
    private baseUri: string;

    constructor( @Inject(HttpClient) private http: HttpClient ) {

        // this.options = HttpHeaders;
        this.url = "https://api.themoviedb.org/3/discover/movie?api_key=2931998c3a80d7806199320f76d65298";
        this.key = "2931998c3a80d7806199320f76d65298";
    }

    /**
     * Get movies
     * GET api/movies
     * @return movies list
     */
    public getMovies(): Observable<Movie[]> {
        return this.http
            .get( this.url )
            .map( this.onGetSuccess )
            .catch( this.onGetUsersError );
    }

    private onGetSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetUsersError( error: any ): Observable<any> {
        const message: string = ( error.message ) ? error.message : error.status ? `${error.status} - ${error.statusText}` : "Server error";
        return Observable.throw( message );
    }
}
