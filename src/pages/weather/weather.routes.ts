import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// IMPORT COMPONENTS
import { WeatherComponent } from "./weather.component";

const WEATHER_ROUTES: Routes = [
    {
        path        : "",
        component   : WeatherComponent,
    },
];

export const RoutingModule: ModuleWithProviders = RouterModule.forChild( WEATHER_ROUTES );
