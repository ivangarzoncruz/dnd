import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

// IMPORT COMPONENTS
import { WeatherComponent } from "./weather.component";

// IMPORT SERVICES
import { WeatherService } from "./weather.service";

// IMPORT ROUTES
import { RoutingModule } from "./weather.routes";

@NgModule({
    imports: [
        CommonModule,
        RoutingModule,
    ],
    declarations: [
        WeatherComponent,
    ],
    providers: [
        WeatherService,
    ],
})
export class WeatherModule { }
