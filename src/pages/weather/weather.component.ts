// Imports
import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";

import { WeatherService } from "./weather.service";

@Component({
    moduleId    : module.id,
    selector    : "app-weather",
    template: `
        <h2>Weather</h2>
        <pre>{{ weather | json }}</pre>
        `,
})
// Component class implementing OnInit
export class WeatherComponent implements OnInit {

    private weather: any;
    private subscription: any;

    constructor( public weatherService: WeatherService ) { }

    public ngOnInit(): void {
        this.getWeather();
    }

    // Load data ones componet is ready
    public getWeather(): void {

        this.subscription = this.weatherService
            .getWeather()
            .subscribe( (weather) => this.weather = weather );
    }

    private ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
