// Imports
import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

// Decorator to tell Angular that this class can be injected as a service to another class
@Injectable()
export class WeatherService {

    private url: string;
    private endpoint: string;
    private weather: any[];

    // Class constructor with Jsonp injected
    constructor( @Inject(HttpClient) private http: HttpClient  ) {

        // Base URL for Petfinder API
        this.url = "https://api.darksky.net/forecast/beaa062ed7297ae069bf8147c418e507/-37.8507240,144.9877640";
    }

    // Get a list if pets based on animal
    public getWeather(): Observable<any[]> {

        let params = new HttpParams();

        // TODO: Add API key globally for all requests to Marvel Entities API
        params = params.append( "callback", "JSON_CALLBACK" );
        params = params.append( "units", "si" );

        return this.http
            .get( this.url , { params } )
            .map( this.onGetUsersSuccess )
            .catch( this.onGetUsersError );
    }

    private onGetUsersSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetUsersError( error: any ): Observable<any> {
        const message: string = ( error.message ) ? error.message : error.status ? `${error.status} - ${error.statusText}` : "Server error";
        return Observable.throw( message );
    }
}
