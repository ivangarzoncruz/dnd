import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { combineLatest } from 'rxjs';
import { IPet as Pet } from "./../../models/pet.model";
import { IType as Type } from "./../../models/type.model";
import { IQuery as Query } from "./../../models/query.model";

import { Store } from "@ngrx/store";
import * as fromStore from "../../store";

import { fadeIn } from "@shared/animations/fade-in";

const TYPES: Type[] = [
    { id: 1, name: "Dog", status: "ACTIVE" },
    { id: 2, name: "Cat", status: "ACTIVE" },
];

/**
 * Container components are permitted to have just enough styles
 * to bring the view together. If the number of styles grow,
 * consider breaking them out into presentational
 * components.
 */
@Component({
    moduleId    : module.id,
    selector    : "app-pets",
    templateUrl : "./pets.component.html",
    styleUrls   : [ "./pets.component.scss" ],
    animations  : [ fadeIn ],
})
export class PetsComponent implements OnInit {

    public animation: any;

    public pets: Pet[];
    public types: Type[] = TYPES;
    public query: Query;

    public pets$ = this.store.select( fromStore.getPetsData );
    public query$ = this.store.select( fromStore.getSearchPetsQuery );
    public search$ = this.store.select( fromStore.getSearchPetsData);
    // public removePet$ = this.store.select( fromStore.getRemoveState );

    // public pets$: Observable<Pet[]>;
    // public query$: Observable<Query>;
    // public removePet$: Observable<any>;

    public petsLoaded$: any;
    public removeLoaded$: any;
    public queryLoaded$: any;

    constructor( protected store: Store<fromStore.PetsState> ) {}

    public ngOnInit(): void {

        this.animation = "void";

        // this.petsLoaded$ = this.pets$
        //     .subscribe( ( pets ) => {
        //         this.pets = pets;
        //     });

        // this.queryLoaded$ = this.query$
        //     .subscribe( ( query ) => {
        //         this.query = query;
        //     });

        // this.pets$.subscribe( ( pets ) => {
        //     this.pets = pets;
        // });
        combineLatest(this.pets$, this.search$).subscribe(val => {
            const resPets = val[0];
            const resSearch = val[1];
            this.pets = resPets || resSearch;
        })

        this.query$.subscribe( ( query ) => {
            this.query = query;
        });
    }

    public handleOnDelete( $e: Pet ): void {

        const confirmDelete = window.confirm(
            "Are you sure you want to delete this Employee from the list? This cannot be undone.",
        );

        if ( confirmDelete ) {
            this.store.dispatch( new fromStore.RemovePet( $e ) );
            // this.subscribeToRemoveChanges();
        }
    }

    public search( $e: any ): void {

        const q = {
          ...this.query,
          search: $e.search,
        };

        console.log( "q ----> ", q );
        this.store.dispatch( new fromStore.SearchPets( q ) );
    }

    public toggleState(): void {
        // console.log( "toggleState" );
        this.animation = ( this.animation === "void" ? "*" : "void" );
    }

    public ngOnDestroy(): void {
        // this.petsLoaded$.unsubscribe();
    }

    // private subscribeToRemoveChanges(): void {

    //     if ( this.removeLoaded$ ) {
    //         return;
    //     }

    //     this.removeLoaded$ = this.removePet$
    //         .subscribe( ( pet ) => {
    //             if ( !pet.loading ) {
    //                 this.store.dispatch( new fromStore.LoadPets() );
    //             }
    //         });
    // }

    // private undo(){
	// 	this._store.dispatch({type: UNDO});
	// }

	// private redo(){
	// 	this._store.dispatch({type: REDO});
	// }
}
