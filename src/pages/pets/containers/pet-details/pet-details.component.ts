import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { tap } from "rxjs/operators";
import * as fromStore from "../../store";

import { IPet as Pet } from "./../../models/pet.model";
import { IType as Type } from "./../../models/type.model";

const TYPES: Type[] = [
    {
        id: 1,
        name: "Dog",
        status: "ACTIVE",
    },
    {
        id: 2,
        name: "Cat",
        status: "ACTIVE",
    },
    {
        id: 3,
        name: "Bunny",
        status: "ACTIVE",
    },
];
@Component({
    moduleId            : module.id,
    selector            : "app-pet-details",
    templateUrl         : "./pet-details.component.html",
    styleUrls           : [ "./pet-details.component.scss" ],
    changeDetection     : ChangeDetectionStrategy.OnPush,
})
export class PetDetailsComponent implements OnInit {

    public pet: Pet;
    public types: Type[] = TYPES;

    public petLoaded$: any;
    public removeLoaded$: any;

    // private pet$ = this.store.select( fromStore.getPetData );
    // private removePet$ = this.store.select( fromStore.getRemoveState );

    pet$: Observable<Pet>;

    constructor(
        private activeRouter: ActivatedRoute,
        protected store: Store<fromStore.PetsState>,
    ) {}

    public ngOnInit() {

        // this.activeRouter.params.subscribe( ( params ) => {
        //     this.store.dispatch( new fromStore.LoadPet( parseInt( params.id, 10 )) );
        // });

        // this.pet$.subscribe( ( pet ) => {
        //     this.pet = pet;
        // });

        this.pet$ = this.store.select( fromStore.getPetData );

        // this.pet$ = this.store.select( fromStore.getSelectedPet );
        this.pet$.subscribe( ( pet ) =>
            this.pet = pet,
        );
    }

    public ngOnDestroy(): void {
        this.store.dispatch( new fromStore.ResetPet() );
    }

    // private subscribeToRemoveChanges(): void {

    //     if ( this.removeLoaded$ ) {
    //         return;
    //     }

    //     this.removeLoaded$ = this.removePet$
    //         .subscribe( ( pet ) => {
    //             if ( !pet.loading ) {
    //                 this.store.dispatch( new fromStore.ResetPet() );
    //                 // console.log( "Deleted pet !!!" );
    //             }
    //         });
    // }
}
