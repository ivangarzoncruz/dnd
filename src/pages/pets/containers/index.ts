
import { PetsComponent } from "./pets/pets.component";
import { PetDetailsComponent } from "./pet-details/pet-details.component";

export const containers: any[] = [
    PetsComponent,
    PetDetailsComponent,
];

export * from "./pets/pets.component";
export * from "./pet-details/pet-details.component";
