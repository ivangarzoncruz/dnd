import {
    EventEmitter,
    Component,
    OnInit,
    Input,
    Output,
    ChangeDetectionStrategy,
} from "@angular/core";

import { IPet as Pet } from "./../../models/pet.model";

@Component({
    moduleId    : module.id,
    selector    : "app-pet-list",
    templateUrl : "./pet-list.component.html",
    styleUrls   : [ "./pet-list.component.scss" ],
    // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PetListComponent implements OnInit {

    @Input()
    public pets: Pet[];

    @Output()
    public edit = new EventEmitter();

    @Output()
    public remove = new EventEmitter();

    @Output()
    public sort = new EventEmitter();

    public order: string = "asc";
    private bodyText: string;

    constructor() { }

    public ngOnInit(): void {
        this.bodyText = "This text can be updated in modal 1";
    }

    editEmployee( $e: Event, id: number ): void {
        this.edit.emit( id );
    }

    deletePet( $e: Event, pet: Pet ): void {

        // console.log( "$e ::: ", $e );
        // console.log( "pet ::: ", pet );

        this.remove.emit( pet );
    }

    toggleSort() {
        this.order = this.order === "asc" ? "desc" : "asc";
        this.sort.emit( this.order );
    }
}
