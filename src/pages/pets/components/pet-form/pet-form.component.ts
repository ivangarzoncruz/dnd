import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ChangeDetectionStrategy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { IType as Type } from "../../models/type.model";
import { IPet as Pet } from "../../models/pet.model";

@Component({
    moduleId         : module.id,
    selector         : "app-pet-form",
    templateUrl      : "./pet-form.component.html",
    styleUrls        : ["./pet-form.component.scss"],
    changeDetection  : ChangeDetectionStrategy.OnPush,
})
export class PetFormComponent implements OnInit, OnChanges {

    public exists = false;

    @Input() pet: Pet;
    @Input() types: Type[];
    @Output() create = new EventEmitter();
    @Output() update = new EventEmitter();
    @Output() remove = new EventEmitter();

    public name: FormControl;
    public breed: FormControl;
    public description: FormControl;
    public type: FormControl;
    public petForm: FormGroup;

    constructor() {}

    public ngOnInit(): void {

        // Set default values based on edit or new mode
        const name = this.exists ? this.pet.name : "";
        const breed = this.exists ? this.pet.breed : "";
        const description = this.exists ? this.pet.description : "";
        const type = this.exists ? this.pet.type : "";

        this.name = new FormControl( name, [ Validators.required ] );
        this.breed = new FormControl( breed, [ Validators.required ] );
        this.description = new FormControl( description, [ Validators.required ] );
        this.type = new FormControl( type, [ Validators.required ] );

        this.petForm = new FormGroup({
            name: this.name,
            breed: this.breed,
            description: this.description,
            type: this.type,
        });
    }

    public ngOnChanges( changes: SimpleChanges ) {
        if ( this.pet && this.pet.id ) {
            this.exists = true;
        }
    }

    public saveEdit( data: any ) {
        if ( !this.exists ) {
            this.create.emit( data );
        } else {
            data.id = this.pet.id;
            this.update.emit( data );
        }
    }

    public validateName() {
        return this.name.valid || this.name.untouched;
    }

    public validateBreed() {
        return this.breed.valid || this.breed.untouched;
    }

    public validateDescription() {
        return this.description.valid || this.description.untouched;
    }

    public deleteEmp( $e: Pet ) {
        this.remove.emit( $e );
    }

    // Compare options to set "selected" on selected option
    public byId( item1: any, item2: any ) {
        return item1.id === item2.id;
    }
}
