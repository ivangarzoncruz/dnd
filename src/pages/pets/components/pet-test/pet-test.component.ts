import { Component, OnInit } from "@angular/core";

@Component({
    moduleId    : module.id,
    selector    : "app-pet-test",
    templateUrl : "./pet-test.component.html",
    styleUrls   : ["./pet-test.component.scss" ],
})
export class PetTestComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
