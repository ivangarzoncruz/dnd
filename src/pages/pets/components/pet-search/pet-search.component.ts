import { Component, OnInit, Output, Input, EventEmitter, ChangeDetectionStrategy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Rx";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

import { IQuery as Query } from "../../models/query.model";
import { IType as Type } from "../../models/type.model";

@Component({
    moduleId        : module.id,
    selector        : "app-pet-search",
    templateUrl     : "./pet-search.component.html",
    styleUrls       : [ "./pet-search.component.scss" ],
    changeDetection : ChangeDetectionStrategy.OnPush,
})
export class PetSearchComponent implements OnInit {

    @Output()
    public search = new EventEmitter<any>();

    @Input()
    public query: Query;

    @Input()
    public types: Type[];

    public values: string = "";
    public debounceTimeSearch = 300;
    public myGroup: FormGroup;
    private searchTerms: Subject<string> = new Subject<string>();

    constructor() { }

    // Push a search term into the observable stream.
    public onKeyUp( value: string ): void {
        this.values = value;
        this.searchTerms.next( this.values );
    }

    public ngOnInit() {

        this.searchTerms.pipe(

                // wait 300ms after each keystroke before considering the term
                debounceTime( this.debounceTimeSearch ),

                // ignore new term if same as previous term
                distinctUntilChanged(),

            // switch to new search observable each time the term changes
            ).subscribe( ( value: string ) => {
                this.onSearchTerm();
            });

        this.myGroup = new FormGroup({
            type: new FormControl(),
            filter: new FormControl(),
        });
    }

    private onSearchTerm(): void {

        // const selectedIndex = this.typeSelector.nativeElement.selectedIndex - 1;
        const selectedIndex = 1;

        this.search.emit({
            search: this.values,
            // type: this.types[ selectedIndex ],
        });

        // console.log( " --- searchTerm --- ", this.values );
    }

    // Compare options to set 'selected' on selected option
    private byId( item1: any, item2: any ) {
        return item2 ? item1.id === item2.id : false;
    }
}
