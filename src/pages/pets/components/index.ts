import { PetListComponent } from "./pet-list/pet-list.component";
import { PetSearchComponent } from "./pet-search/pet-search.component";
import { PetFormComponent } from "./pet-form/pet-form.component";
import { PetTestComponent } from "./pet-test/pet-test.component";

export const components: any[] = [
    PetListComponent,
    PetSearchComponent,
    PetFormComponent,
    PetTestComponent,
];

export * from "./pet-list/pet-list.component";
export * from "./pet-search/pet-search.component";
export * from "./pet-form/pet-form.component";
export * from "./pet-test/pet-test.component";
