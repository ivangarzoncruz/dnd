import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

/** IMPORT MODULES */
import { SharedModule } from "@shared/shared.module";

import { reducers, effects } from "./store";

// Containers
import * as fromContainers from "./containers";

// Components
import * as fromComponents from "./components";

// Services
import * as fromServices from "./services";

// Guards
import * as fromGuards from "./guards";

// For InMemory testing
import { HttpClientModule } from "@angular/common/http";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { PetsFakeBackend } from "./pets-fake-backend.service";

// Routes
import { RoutingModule } from "./pets.routes";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        RoutingModule,
        HttpClientModule,
        StoreModule.forFeature( "managePets", reducers ),
        EffectsModule.forFeature( effects ),
        InMemoryWebApiModule.forRoot(
            PetsFakeBackend, {
                dataEncapsulation: false,
                delay: 0,
            },
        ),
    ],
    declarations: [
        ...fromComponents.components,
        ...fromContainers.containers,
    ],
    providers: [
        ...fromServices.services,
        ...fromGuards.guards,
    ],
})
export class PetsModule {}
