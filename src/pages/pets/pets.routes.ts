import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import * as fromContainers from "./containers";
import * as fromComponents from "./components";
import * as fromGuards from "./guards";

const PETS_ROUTES: Routes = [
    {
        path         : "",
        component    : fromContainers.PetsComponent,
        canActivate: [ fromGuards.PetsGuard ],
    },
    {
        path         : "new",
        component    : fromContainers.PetDetailsComponent,
    },
    {
        path         : ":id",
        component    : fromContainers.PetDetailsComponent,
        canActivate: [ fromGuards.PetExistsGuard, fromGuards.PetDetailsExistsGuard ],
    },
];

export const RoutingModule: ModuleWithProviders = RouterModule.forChild( PETS_ROUTES );
