import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromFeature from "../reducers";
import * as fromPets from "../reducers/pets.reducer";

import * as fromRoot from "../../../../core/store/";
import { IPet as Pet } from "./../../models/pet.model";

// export interface PowersState {
// powers: fromPowers.State;
// }

// export interface State extends AppState {
// powers: PowersState;
// }

export const getPetsState = createSelector(
    fromFeature.getManagePetsState,
    (state: fromFeature.PetsState) => state.pets,
);

// export const getPetsState = (state: fromFeature.PetsState) => state.pets;
export const getPetsEntities = createSelector( getPetsState, fromPets.getPetsEntities );
export const getPetsLoaded = createSelector( getPetsState, fromPets.getPetsLoaded );
export const getPetsLoading = createSelector( getPetsState, fromPets.getPetsLoading );
export const getPetsFailed = createSelector( getPetsState, fromPets.getPetsFailed );
export const getPetsQuery = createSelector( getPetsState, fromPets.getPetsQuery );
export const getPetsData = createSelector( getPetsState, fromPets.getPetsData );

// export const getPetsAll = createSelector(
//     getPetsEntities,
//     ( entities ) => {
//         return Object.keys( entities ).map( ( id ) => entities[ +id ] );
//     },
// );

// export const getSelectedPetId = createSelector(
//     getPetsEntities,
//     fromPets.getSelectedPetId,
// );

// export const getSelectedPet = createSelector(
//     getPetsEntities,
//     fromRoot.getRouterState,
//     ( entities, router ): Pet => {
//         return router && router.state && entities[ router.state.params.id ];
//     },
// );
