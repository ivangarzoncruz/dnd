import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromFeature from "../reducers";
import * as fromSearch from "../reducers/search.reducer";

// import * as fromRoot from "../../../../core/store/";
// import { IPet as Pet } from "./../../models/pet.model";

// export interface PowersState {
// powers: fromPowers.State;
// }

// export interface State extends AppState {
// powers: PowersState;
// }

export const getSearchState = createSelector(
    fromFeature.getManagePetsState,
    (state: fromFeature.PetsState) => state.pets,
);

// export const getPetsState = (state: fromFeature.PetsState) => state.pets;
export const getSearchEntities = createSelector( getSearchState, fromSearch.getSearchEntities );
export const getSearchPetsLoaded = createSelector( getSearchState, fromSearch.getSearchLoaded );
export const getSearchPetsLoading = createSelector( getSearchState, fromSearch.getSearchLoading );
export const getSearchPetsFailed = createSelector( getSearchState, fromSearch.getSearchFailed );
export const getSearchPetsQuery = createSelector( getSearchState, fromSearch.getSearchQuery );
export const getSearchPetsData = createSelector( getSearchState, fromSearch.getSearchData );

/**
 * Some selector functions create joins across parts of state. This selector
 * composes the search result IDs to return an array of employees in the store.
 */
// export const getSearchResults = createSelector( getSearchEntities, getSearchPetsData, ( pets, data ) => {
//     return pets.map( id => data[ id ]);
// });
