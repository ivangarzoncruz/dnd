import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromRoot from "../../../../core/store/";
import * as fromFeature from "../reducers";
import * as fromPet from "../reducers/pet.reducer";
import * as fromPets from "../reducers/pets.reducer";

import { IPet as Pet } from "./../../models/pet.model";

export const getPetState = createSelector(
    fromFeature.getManagePetsState,
    (state: fromFeature.PetsState) => state.pet,
);

/**
 * Pet store functions
 */
export const getPetData = createSelector( getPetState, fromPet.getPetData );
export const getPetLoaded = createSelector( getPetState, fromPet.getPetLoaded );
export const getPetLoading = createSelector( getPetState, fromPet.getPetLoading );
export const getPetFailed = createSelector( getPetState, fromPet.getPetFailed );

// export const getSelectedPet = createSelector(
//     fromPets.getPetsEntities,
//     fromRoot.getRouterState,
//     (entities, router): Pet => {
//         return router && router.state && entities[ router.state.params.id ];
//     },
// );
