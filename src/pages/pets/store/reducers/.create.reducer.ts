// import { IPet as Pet } from "../../models/pet.model";
// import * as fromCreate from "./../actions/create.action";

// export interface CreateState {
//     entities: { [id: number]: Pet };
//     loaded: boolean;
//     loading: boolean;
//     failed: boolean;
//     data: Pet[];
// }

// const INITIAL_STATE: CreateState = {
//     entities: {},
//     loaded: false,
//     loading: false,
//     failed: false,
//     data: [],
// };

// export function reducer( state = INITIAL_STATE, action: fromCreate.CreateActions ): CreateState {
//     if ( !action ) {
//         return state;
//     }

//     switch ( action.type ) {
//         case fromCreate.CreateActionTypes.CREATE_PET_LOAD: {
//             return Object.assign({}, state, {
//                 loading: true,
//             });
//         }

//         case fromCreate.CreateActionTypes.CREATE_PET_SUCCESS: {

//             // const pet = action.payload;
//             // // console.log( "REMOVE_PET_SUCCESS ---> ", pet );

//             // return Object.assign({}, state, {
//             //     loading    : false,
//             //     failed    : false,
//             // });

//             const pet = action.payload;

//             const entities = {
//                 ...state.entities,
//                 [ pet.id ]: pet,
//             };

//             const data = state.data;
//             const index = data.findIndex( (p) => p.id === pet.id );

//             data[ index ] = pet;

//             return {
//                 ...state,
//                 entities,
//                 data,
//                 loaded    : true,
//                 loading   : false,
//                 failed    : false,
//             };
//         }

//         case fromCreate.CreateActionTypes.CREATE_PET_FAIL: {
//             return Object.assign({}, state, {
//                 loading   : false,
//                 failed    : true,
//             });
//         }

//         default: {
//             return state;
//         }
//     }
// }

// export const getLoading = ( state: CreateState ) => state.loading;
// export const getFailed = ( state: CreateState ) => state.failed;
