import { IPet as Pet } from "../../models/pet.model";
import { IQuery as Query } from "../../models/query.model";

import * as fromPets from "./../actions/pets.action";

export interface PetState {
    entities: { [ id: string ]: Pet };
    loaded: boolean;
    loading: boolean;
    failed: boolean;
    query: Query;
    data: Pet[];
}

const INITIAL_STATE: PetState = {
    entities: {},
    loaded: false,
    loading: false,
    failed: false,
    query: {
        search: "",
        sort: {
            order: "asc",
            type: "fname",
        },
    },
    data: [],
};

export function reducer( state = INITIAL_STATE, action: fromPets.PetsAction ): PetState {

    if ( !action ) {
      return state;
    }

    switch (action.type) {

        case fromPets.ActionTypes.LOAD_PETS: {
            return Object.assign({}, state, {
                loading: true,
            });
        }

        case fromPets.ActionTypes.LOAD_PETS_SUCCESS: {

            const pets = action.payload;

            const entities = pets.reduce(
                ( entities : { [ id: number ]: Pet }, pet: Pet ) => {
                    return {
                        ...entities,
                        [ pet.id ] : pet,
                    };
                },
                {
                    ...state.entities,
                },
            );

            return {
                ...state,
                entities,
                loaded  : true,
                loading : false,
                failed  : false,
                data    : pets,
            };
        }

        case fromPets.ActionTypes.LOAD_PETS_FAIL: {
            return Object.assign({}, state, {
                loaded: false,
                loading: false,
                failed: true,
                data: [],
            });
        }

        // case fromPets.ActionTypes.SEARCH_PETS: {
        //     return Object.assign({}, state, {
        //         loading: true,
        //     });
        // }

        // case fromPets.ActionTypes.SEARCH_PETS_SUCCESS: {

        //     const pets = action.payload.pets;
        //     const q = action.payload.query;

        //     const entities = pets.reduce(
        //         ( entities : { [ id: number ]: Pet }, pet: Pet ) => {
        //             return {
        //                 ...entities,
        //                 [ pet.id ] : pet,
        //             };
        //         },
        //         {},
        //     );

        //     return Object.assign({}, state, {
        //         entities,
        //         loaded    : true,
        //         loading   : false,
        //         failed    : false,
        //         data      : pets,
        //         query     : q,
        //     });
        // }

        // case fromPets.ActionTypes.SEARCH_PETS_FAIL: {

        //     const q = action.payload.query;

        //     return Object.assign({}, state, {
        //         loaded    : false,
        //         loading   : false,
        //         failed    : true,
        //         data      : [],
        //         query     : q,
        //     });
        // }

        case fromPets.ActionTypes.REMOVE_PET: {
            return Object.assign({}, state, {
                loading: true,
            });
        }

        case fromPets.ActionTypes.REMOVE_PET_SUCCESS: {

            const pet: Pet = action.payload;
            const { [ pet.id ]: removed, ...entities } = state.entities;

            const data = state.data.filter( (p) => {
                return p.id !== pet.id;
            });

            return {
                ...state,
                entities,
                data,
                loading: false,
                failed: false,
            };
        }

        case fromPets.ActionTypes.REMOVE_PET_FAIL: {
            return Object.assign({}, state, {
                loading: false,
                failed: true,
            });
        }

        default: {
            return state;
        }
    }
}

/**
 * Returns pets entities.
 * @function getPetsEntities
 * @param {State} state
 * @returns {array}
 */

export const getPetsEntities = ( state: PetState ) => state.entities;

/**
 * Returns true if pets are loaded.
 * @function getPetsLoaded
 * @param {State} state
 * @returns {boolean}
 */
export const getPetsLoaded = ( state: PetState ) => state.loaded;

/**
 * Returns true if pets are loaded successfully.
 * @function getPetsLoading
 * @param {State} state
 * @returns {boolean}
 */
export const getPetsLoading = ( state: PetState ) => state.loading;

/**
 * Returns false if pets failed to load.
 * @function getPetsFailed
 * @param {State} state
 * @returns {boolean}
 */
export const getPetsFailed = ( state: PetState ) => state.failed;

/**
 * Returns query parameters.
 * @function getPetsQuery
 * @param {State} state
 * @returns {object}
 */
export const getPetsQuery = ( state: PetState ) => state.query;

/**
 * Returns full pets object.
 * @function getPetsData
 * @param {State} state
 * @returns {object}
 */
export const getPetsData = ( state: PetState ) => state.data;
