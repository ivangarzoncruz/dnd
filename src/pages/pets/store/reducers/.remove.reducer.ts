// import { IPet as Pet } from "../../models/pet.model";
// import * as fromRemove from "./../actions/remove.action";
// // import { PetsState } from ".";

// export interface RemoveState {
//     entities: { [id: number]: Pet };
//     loaded: boolean;
//     loading: boolean;
//     failed: boolean;
// }

// const INITIAL_STATE: RemoveState = {
//     entities   : {},
//     loaded     : false,
//     loading    : false,
//     failed     : false,
// };

// export function reducer( state = INITIAL_STATE, action: fromRemove.RemoveActions ): RemoveState {
//     if ( !action ) {
//         return state;
//     }

//     switch ( action.type ) {
//         case fromRemove.RemoveActionTypes.REMOVE_PET: {
//             return Object.assign({}, state, {
//                 loading: true,
//             });
//         }

//         case fromRemove.RemoveActionTypes.REMOVE_PET_SUCCESS: {

//             const pet = action.payload;
//             const { [ pet.id ]: removed, ...entities } = state.entities;

//             return {
//                 ...state,
//                 entities,
//                 loading: false,
//                 failed: false,
//             };
//         }

//         case fromRemove.RemoveActionTypes.REMOVE_PET_FAIL: {
//             return Object.assign({}, state, {
//                 loading: false,
//                 failed: true,
//             });
//         }

//         default: {
//             return state;
//         }
//     }
// }

// export const getEntities = ( state: RemoveState ) => state.entities;
// export const getLoading = ( state: RemoveState ) => state.loading;
// export const getFailed = ( state: RemoveState ) => state.failed;
