import { IPet as Pet } from "../../models/pet.model";
import * as fromPet from "./../actions/pet.action";

export interface PetState {
    loaded: boolean;
    loading: boolean;
    failed: boolean;
    data: any;
}

const INITIAL_STATE: PetState = {
    loaded: false,
    loading: false,
    failed: false,
    data: {},
};

export function reducer( state = INITIAL_STATE, action: fromPet.PetActions ): PetState {
    if ( !action ) {
        return state;
    }

    switch ( action.type ) {
        case fromPet.PetActionTypes.LOAD_PET: {
            return Object.assign({}, state, {
                loading: true,
            });
        }

        case fromPet.PetActionTypes.LOAD_PET_SUCCESS: {

            const pet = action.payload;

            return Object.assign({}, state, {
                loaded    : true,
                loading   : false,
                failed    : false,
                data      : pet,
            });
        }

        case fromPet.PetActionTypes.LOAD_PET_FAIL: {
            return Object.assign({}, state, {
                loaded    : false,
                loading   : false,
                failed    : true,
                data      : {},
            });
        }

        case fromPet.PetActionTypes.RESET_PET: {
            return Object.assign({}, INITIAL_STATE);
            // return Object.assign({}, state, {
            //     loaded    : false,
            //     loading   : false,
            //     failed    : true,
            //     data      : {},
            // });
        }

        default: {
            return state;
        }
    }
}

export const getPetData = ( state: PetState ) => state.data;
export const getPetLoaded = ( state: PetState ) => state.loaded;
export const getPetLoading = ( state: PetState ) => state.loading;
export const getPetFailed = ( state: PetState ) => state.failed;
