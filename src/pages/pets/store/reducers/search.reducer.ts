import { IPet as Pet } from "../../models/pet.model";
import { IQuery as Query } from "../../models/query.model";
import * as fromSearch from "./../actions/search.actions";
import { Action } from "@ngrx/store";
export interface SearchState {
    entities: { [id: string]: Pet };
    loaded: boolean;
    loading: boolean;
    failed: boolean;
    query: Query;
    data: Pet[];
}

const INITIAL_STATE: SearchState = {
    entities: {},
    loaded: false,
    loading: false,
    failed: false,
    query: {
        search: "",
        sort: {
            order: "asc",
            type: "fname",
        },
    },
    data: [],
};

export function reducer( state = INITIAL_STATE, action: fromSearch.SearchAction ): SearchState {

    if ( !action ) {
        return state;
    }

    switch (action.type) {

        case fromSearch.SearchActionTypes.SEARCH_PETS: {
            return Object.assign({}, state, {
                loading: true,
            });
        }

        case fromSearch.SearchActionTypes.SEARCH_PETS_SUCCESS: {
            const pets: Pet[] = action.payload['pets'];
            const q: Query = action.payload['query'];

            const entities = pets.reduce(
                ( entities : { [ id: number ]: Pet }, pet: Pet ) => {
                    return {
                        ...entities,
                        [ pet.id ] : pet,
                    };
                },
                {},
            );

            return Object.assign({}, state, {
                entities,
                loaded    : true,
                loading   : false,
                failed    : false,
                data      : pets,
                query     : q,
            });
        }

        case fromSearch.SearchActionTypes.SEARCH_PETS_FAIL: {

            const q = action.payload['query'];

            return Object.assign({}, state, {
                loaded    : false,
                loading   : false,
                failed    : true,
                data      : [],
                query     : q,
            });
        }
    }
}

export const getSearchEntities = ( state: SearchState ) => state.entities;
export const getSearchLoaded = ( state: SearchState ) => state.loaded;
export const getSearchLoading = ( state: SearchState ) => state.loading;
export const getSearchFailed = ( state: SearchState ) => state.failed;
export const getSearchQuery = ( state: SearchState ) => state.query;
export const getSearchData = ( state: SearchState ) => state.data;
