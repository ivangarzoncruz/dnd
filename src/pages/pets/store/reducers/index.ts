import { ActionReducer, ActionReducerMap, createSelector, createFeatureSelector } from "@ngrx/store";
import { storeFreeze } from "ngrx-store-freeze";
import { compose } from "@ngrx/core/compose";
import { combineReducers } from "@ngrx/store";
import { environment } from "@env/environment";

/** Reducers */
import * as fromPets from "./pets.reducer";
import * as fromPet from "./pet.reducer";
import * as fromSearch from "./search.reducer";
// import * as fromRemove from "./remove.reducer";
// import * as fromCreate from "./create.reducer";

export interface PetsState {
    pets: fromPets.PetState;
    pet: fromPet.PetState;
    search: fromSearch.SearchState;
    // remove: fromRemove.RemoveState;
    // create: fromCreate.CreateState;
}

export const reducers: ActionReducerMap<PetsState> = {
    pets    : fromPets.reducer,
    pet     : fromPet.reducer,
    search  : fromSearch.reducer,
    // remove  : fromRemove.reducer,
    // create  : fromCreate.reducer,
};

export const getManagePetsState = createFeatureSelector<PetsState>( "managePets" );
