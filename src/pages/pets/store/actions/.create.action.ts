// import { Action } from "@ngrx/store";
// // import { type } from "@shared/utils/utils";
// import { IPet as Pet } from "./../../models/pet.model";

// export const CreateActionTypes = {
//   // CREATE_PET_LOAD        : type( "[ Pet ] Create Load" ),
//   CREATE_PET_LOAD        : "[ Pet ] Create Load",
//   CREATE_PET_SUCCESS     : "[ Pet ] Create Load Success",
//   CREATE_PET_FAIL        : "[ Pet ] Create Load Fail",
// };

// /**
//  * Create Actions
//  */
// export class CreatePetLoadAction implements Action {
//   type = CreateActionTypes.CREATE_PET_LOAD;
//   constructor(public payload: Pet) { }
// }

// export class CreatePetLoadSuccessAction implements Action {
//   type = CreateActionTypes.CREATE_PET_SUCCESS;
//   constructor(public payload: Pet) { }
// }

// export class CreatePetLoadFailAction implements Action {
//   type = CreateActionTypes.CREATE_PET_FAIL;
//   constructor(public payload: any = null) { }
// }

// export type CreateActions
//   = CreatePetLoadAction
//   | CreatePetLoadSuccessAction
//   | CreatePetLoadFailAction;
