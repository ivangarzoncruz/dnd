import { Action } from "@ngrx/store";

import { IPet as Pet } from "../../models/pet.model";
import { IQuery as Query } from "../../models/query.model";

// load employees
export const SearchActionTypes = {
    SEARCH_PETS               : "[ Pets ] Search Pets",
    SEARCH_PETS_FAIL          : "[ Pets ] Search Pets Fail",
    SEARCH_PETS_SUCCESS       : "[ Pets ] Search Pets Success",
};

/**
 * Checks if search exist.
 * @class SearchPets
 * @implements {Action}
 */
export class SearchPets implements Action {
    readonly type = SearchActionTypes.SEARCH_PETS;
    constructor(public payload: any) {
        console.log( "payload ---> ", payload );
    }
}

/**
 * Search Pets check fails.
 * @class SearchPetsFail
 * @implements {Action}
 */
export class SearchPetsFail implements Action {
    readonly type = SearchActionTypes.SEARCH_PETS_FAIL;
    constructor(public payload: Query) {}
}

/**
 * Search Pets check success.
 * @class SearchPetsSuccess
 * @implements {Action}
 */
export class SearchPetsSuccess implements Action {
    readonly type = SearchActionTypes.SEARCH_PETS_SUCCESS;
    constructor(public payload: { pets: Pet[]; query: Query }) {}
}

/**
 * Actions type.
 * @type {Actions}
 */
export type SearchAction =
  | SearchPets
  | SearchPetsFail
  | SearchPetsSuccess;
