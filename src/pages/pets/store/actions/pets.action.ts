import { Action } from "@ngrx/store";

import { IPet as Pet } from "../../models/pet.model";
import { IQuery as Query } from "../../models/query.model";

// load employees
export const ActionTypes = {
    LOAD_PETS                 : "[ Pets ] Load Pets",
    LOAD_PETS_FAIL            : "[ Pets ] Load Pets Fail",
    LOAD_PETS_SUCCESS         : "[ Pets ] Load Pets Success",
    // SEARCH_PETS               : "[ Pets ] Search Pets",
    // SEARCH_PETS_FAIL          : "[ Pets ] Search Pets Fail",
    // SEARCH_PETS_SUCCESS       : "[ Pets ] Search Pets Success",
    REMOVE_PET                : "[ Pets ] Remove Pet",
    REMOVE_PET_FAIL           : "[ Pets ] Remove Pet Fail",
    REMOVE_PET_SUCCESS        : "[ Pets ] Remove Pet Success",
};

/**
 * Load Pets.
 * @class LoadPets
 * @implements {Action}
 */
export class LoadPets implements Action {
    readonly type = ActionTypes.LOAD_PETS;
    constructor(public payload: any = null) {}
}

/**
 * Load Pets check fails.
 * @class LoadPetsFail
 * @implements {Action}
 */
export class LoadPetsFail implements Action {
    readonly type = ActionTypes.LOAD_PETS_FAIL;
    constructor(public payload: any = null) {}
}

/**
 * Load Pets check success.
 * @class LoadPetsSuccess
 * @implements {Action}
 */
export class LoadPetsSuccess implements Action {
    readonly type = ActionTypes.LOAD_PETS_SUCCESS;
    constructor(public payload: Pet[]) {}
}

// /**
//  * Checks if search exist.
//  * @class SearchPets
//  * @implements {Action}
//  */
// export class SearchPets implements Action {
//     readonly type = ActionTypes.SEARCH_PETS;
//     constructor(public payload: Query) {}
// }

// /**
//  * Search Pets check fails.
//  * @class SearchPetsFail
//  * @implements {Action}
//  */
// export class SearchPetsFail implements Action {
//     readonly type = ActionTypes.SEARCH_PETS_FAIL;
//     constructor(public payload: Query) {}
// }

// /**
//  * Search Pets check success.
//  * @class SearchPetsSuccess
//  * @implements {Action}
//  */
// export class SearchPetsSuccess implements Action {
//     readonly type = ActionTypes.SEARCH_PETS_SUCCESS;
//     constructor(public payload: { pets: Pet[]; query: Query }) {}
// }

export class RemovePet implements Action {
    readonly type = ActionTypes.REMOVE_PET;
    constructor(public payload: Pet) {}
}

export class RemovePetSuccess implements Action {
    readonly type = ActionTypes.REMOVE_PET_SUCCESS;
    constructor(public payload: Pet) {}
}

export class RemovePetFail implements Action {
    readonly type = ActionTypes.REMOVE_PET_FAIL;
    constructor(public payload: any = null) {}
}

/**
 * Actions type.
 * @type {Actions}
 */
export type PetsAction =
  | LoadPets
  | LoadPetsFail
  | LoadPetsSuccess
  | RemovePet
  | RemovePetSuccess
  | RemovePetFail;
