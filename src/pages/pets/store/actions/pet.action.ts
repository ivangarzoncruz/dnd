import { Action } from "@ngrx/store";
import { IPet as Pet } from "../../models/pet.model";

export const PetActionTypes = {
    LOAD_PET            : "[ Pet ] Load Pet",
    LOAD_PET_FAIL       : "[ Pet ] Load Pet Fail",
    LOAD_PET_SUCCESS    : "[ Pet ] Load Pet Success",
    RESET_PET           : "[ Pet ] Reset Pet",
};

export class LoadPet implements Action {
    readonly type = PetActionTypes.LOAD_PET;
    constructor(public payload: number) { }
}

export class LoadPetSuccess implements Action {
    readonly type = PetActionTypes.LOAD_PET_SUCCESS;
    constructor(public payload: Pet) { }
}

export class LoadPetFail implements Action {
    readonly type = PetActionTypes.LOAD_PET_FAIL;
    constructor(public payload: any = null) { }
}

export class ResetPet implements Action {
    readonly type = PetActionTypes.RESET_PET;
    constructor(public payload: any = null) { }
}

export type PetActions
  = LoadPet
  | LoadPetSuccess
  | LoadPetFail
  | ResetPet;
