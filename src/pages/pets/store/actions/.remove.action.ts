// import { Action } from "@ngrx/store";
// import { IPet as Pet } from "../../models/pet.model";

// export const RemoveActionTypes = {
//     REMOVE_PET          : "[ Pet ] Remove Pet",
//     REMOVE_PET_FAIL     : "[ Pet ] Remove Pet Fail",
//     REMOVE_PET_SUCCESS  : "[ Pet ] Remove Pet Success",
// };

// /**
//  * Remove Pet Actions
//  */
// export class RemovePet implements Action {
//     readonly type = RemoveActionTypes.REMOVE_PET;
//     constructor(public payload: Pet) {}
// }

// export class RemovePetSuccess implements Action {
//     readonly type = RemoveActionTypes.REMOVE_PET_SUCCESS;
//     constructor(public payload: Pet) {}
// }

// export class RemovePetFail implements Action {
//     readonly type = RemoveActionTypes.REMOVE_PET_FAIL;
//     constructor(public payload: any = null) {}
// }

// export type RemoveActions
//   = RemovePet
//   | RemovePetSuccess
//   | RemovePetFail;
