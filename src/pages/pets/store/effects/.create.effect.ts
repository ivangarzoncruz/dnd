// import { Injectable } from "@angular/core";

// import { Effect, Actions } from "@ngrx/effects";
// import { of } from "rxjs/observable/of";
// import { map, switchMap, catchError } from "rxjs/operators";

// import * as createActions from "../actions/create.action";
// import * as fromServices from "../../services";

// @Injectable()
// export class CreateEffects {

//     /**
//      * Create Pet effect
//      */
//     @Effect()
//     createPet$ = this.actions$.ofType( createActions.CreateActionTypes.CREATE_PET_LOAD )
//         .pipe(
//             map((action: createActions.CreatePetLoadAction) => action.payload),
//             switchMap( ( pet ) => {
//                 return this.petsService
//                     .createPet( pet )
//                     .pipe(
//                         map( ( p ) =>
//                             new createActions.CreatePetLoadSuccessAction( p ),
//                         ),
//                         catchError( ( error ) =>
//                             of(new createActions.CreatePetLoadFailAction( error )),
//                         ),
//                     );
//             }),
//         );

//     constructor( private actions$: Actions, private petsService: fromServices.PetsService ) {}
// }
