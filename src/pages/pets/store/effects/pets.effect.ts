import { Injectable } from "@angular/core";

import { Effect, Actions } from "@ngrx/effects";
import { of } from "rxjs";
import { map, switchMap, catchError } from "rxjs/operators";

import * as petsActions from "../actions/pets.action";
import * as fromServices from "../../services";

@Injectable()
export class PetsEffects {

    /**
     * Load Pets effect
     */
    @Effect()
    loadPets$ = this.actions$.ofType( petsActions.ActionTypes.LOAD_PETS ).pipe(
        switchMap(() => {
            return this.petsService
                .getPets()
                .pipe(
                    map(( pets ) => {
                            return new petsActions.LoadPetsSuccess( pets );
                        },
                    ),
                    catchError( ( error ) =>
                        of( new petsActions.LoadPetsFail( error ) ),
                    ),
                );
        }),
    );

    // @Effect()
    // searchPets$ = this.actions$.ofType( petsActions.ActionTypes.SEARCH_PETS ).pipe(
    //     map(( action: petsActions.SearchPets ) => action.payload ),
    //     switchMap( ( query ) => {
    //         return this.petsService
    //             .searchPets( query )
    //             .pipe(
    //                 map(
    //                     ( pets ) => new petsActions.SearchPetsSuccess({ pets, query }),
    //                 ),
    //                 catchError( ( error ) =>
    //                     of( new petsActions.SearchPetsFail( error ) ),
    //                 ),
    //             );
    //     }),
    // );

    /**
     * Remove Pet effect
     */
    @Effect()
    removePet$ = this.actions$.ofType( petsActions.ActionTypes.REMOVE_PET )
        .pipe(
            map(( action: petsActions.RemovePet ) => action.payload ),
            switchMap( ( pet ) => {
                return this.petsService
                    .removePet( pet )
                    .pipe(
                        map( () =>
                            new petsActions.RemovePetSuccess( pet ),
                        ),
                        catchError( ( error ) =>
                            of( new petsActions.RemovePetFail( error ) ),
                        ),
                    );
            }),
        );

    constructor( private actions$: Actions, private petsService: fromServices.PetsService ) {}

}
