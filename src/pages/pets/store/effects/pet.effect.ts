import { Injectable } from "@angular/core";

import { Effect, Actions } from "@ngrx/effects";
import { of } from "rxjs";
import { map, switchMap, catchError } from "rxjs/operators";

import * as petActions from "../actions/pet.action";
import * as fromServices from "../../services";

@Injectable()
export class PetEffects {

    /**
     * Load Pet effect
     */
    @Effect()
    loadPet$ = this.actions$.ofType( petActions.PetActionTypes.LOAD_PET )
        .pipe(
            map(( action: petActions.LoadPet ) => action.payload ),
            switchMap(( pet ) => {
                return this.petsService
                    .getPetById( pet )
                    .pipe(
                        map( ( d ) =>
                            new petActions.LoadPetSuccess( d ),
                        ),
                        catchError( ( error ) =>
                            of( new petActions.LoadPetFail( error )),
                        ),
                    );
            }),
        );

    /**
     * @constructor
     * @param {actions$} Actions
     * @param {petsService} fromServices.PetsService
     */
    constructor( private actions$: Actions, private petsService: fromServices.PetsService ) {}
}
