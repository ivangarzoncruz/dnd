// import { Injectable } from "@angular/core";

// import { Effect, Actions } from "@ngrx/effects";
// import { of } from "rxjs/observable/of";
// import { map, switchMap, catchError } from "rxjs/operators";

// import * as removeActions from "../actions/remove.action";
// import * as fromServices from "../../services";

// @Injectable()
// export class RemoveEffects {

//     /**
//      * Remove Pet effect
//      */
//     @Effect()
//     removePet$ = this.actions$.ofType( removeActions.RemoveActionTypes.REMOVE_PET )
//         .pipe(
//             map(( action: removeActions.RemovePet ) => action.payload ),
//             switchMap( ( pet ) => {
//                 return this.petsService
//                     .removePet( pet )
//                     .pipe(
//                         map( () =>
//                             new removeActions.RemovePetSuccess( pet ),
//                         ),
//                         catchError( ( error ) =>
//                             of( new removeActions.RemovePetFail( error ) ),
//                         ),
//                     );
//             }),
//         );

//     constructor( private actions$: Actions, private petsService: fromServices.PetsService ) {}
// }
