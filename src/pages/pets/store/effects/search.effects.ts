import { Injectable } from "@angular/core";

import { Effect, Actions } from "@ngrx/effects";
import { of } from "rxjs";
import { map, switchMap, catchError } from "rxjs/operators";

import * as searchActions from "../actions/search.actions";
import * as fromServices from "../../services";

@Injectable()
export class SearchEffects {

    @Effect()
    searchPets$ = this.actions$.ofType( searchActions.SearchActionTypes.SEARCH_PETS ).pipe(
        map(( action: searchActions.SearchPets ) => action.payload ),
        switchMap( ( query ) => {
            return this.petsService
                .searchPets( query )
                .pipe(
                    map(
                        ( pets ) => new searchActions.SearchPetsSuccess({ pets, query }),
                    ),
                    catchError( ( error ) =>
                        of( new searchActions.SearchPetsFail( error ) ),
                    ),
                );
        }),
    );

    constructor( private actions$: Actions, private petsService: fromServices.PetsService ) {}
}
