import { PetsEffects } from "./pets.effect";
import { PetEffects } from "./pet.effect";
import { SearchEffects } from "./search.effects";
// import { RemoveEffects } from "./remove.effect";
// import { CreateEffects } from "./create.effect";

export const effects: any[] = [
    PetsEffects,
    PetEffects,
    SearchEffects,
    // RemoveEffects,
    // CreateEffects,
];

export * from "./pets.effect";
export * from "./pet.effect";
export * from "./search.effects";
// export * from "./create.effect";
