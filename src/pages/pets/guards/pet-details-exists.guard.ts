import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs";
import { tap, map, filter, take, switchMap, catchError } from "rxjs/operators";

import * as fromStore from "../store";
import { IPet as Pet } from "./../models/pet.model";

/**
 * Guards are hooks into the route resolution process, providing an opportunity
 * to inform the router"s navigation process whether the route should continue
 * to activate this route. Guards must return an observable of true or false.
 */
@Injectable()
export class PetDetailsExistsGuard implements CanActivate {
    constructor(
        private store: Store<fromStore.PetsState>,
        private router: Router,
    ) { }

    /**
     * @name waitForPetsToLoad
     * @description    This method creates an observable that waits for the `loaded` property
     *                 of the pets state to turn `true`, emitting one time once loading.
     *
     * @returns {Observable<boolean>} The pet observable.
     */
    waitForPetToLoad(): Observable<boolean> {
        return this.store.select( fromStore.getPetLoaded )
            .pipe(
                map( ( loaded ) => loaded ),
                take(1),
            );
    }

    /**
     * @name hasPetInStore
     * @param {string} id
     * @description    This method checks if a pet with the given ID
     *                 is already registered in the Store.
     *
     * @returns {Observable<boolean>} The pet observable.
     */
    hasPetDetailsInStore( id: string ): Observable<boolean> {
        return this.store.select( fromStore.getPetData )
            .pipe(
                map( ( pet: any ) =>
                    pet.id === id,
                ),
                take(1),
            );
    }

    /**
     * @name hasPetInApi
     * @param {string} id
     * @description    This method loads a pet with the given ID from
     *                 the API and caches it in the store, returning
     *                 `true` or `false` if it was found.
     *
     * @returns {Observable<boolean>} The pet observable.
     */
    hasPetDetailsInApi( id: string ): Observable<boolean> {
        return this.store.select( fromStore.getPetLoaded ).pipe(
            tap( ( loaded ) => {
                if ( !loaded ) {
                    this.store.dispatch( new fromStore.LoadPet(+id));
                }
            }),
            filter( (loaded) => loaded),
            take(1),
        );
    }

    /**
     * @name hasPet
     * @param {string} id
     * @description    `hasPet` composes `hasPetInStore` and `hasPetInApi`.
     *                 It first checks if the pet is in store, and if not
     *                 it then checks if it is in the API.
     *
     * @returns {Observable<boolean>} The pet observable.
     */
    hasPetDetails( id: string ): Observable<boolean> {

        return this.hasPetDetailsInStore( id )
            .pipe(
                switchMap( ( inStore ) => {

                    // debugger
                    if ( inStore ) {
                        return of( inStore );
                    }

                    return this.hasPetDetailsInApi(id);
                }),
            );
    }

    /**
     * @name canActivate
     * @param {route} ActivatedRouteSnapshot
     * @description
     * This is the actual method the router will call when our guard is run.
     *
     * Our guard waits for the pet collection to load, then it checks if we need
     * to request a book from the API or if we already have it in our cache.
     * If it finds it in the cache or in the API, it returns an Observable
     * of `true` and the route is rendered successfully.
     *
     * If it was unable to find it in our cache or in the API, this guard
     * will return an Observable of `false`, causing the router to move
     * on to the next candidate route. In this case, it will move on
     * to the 404 page.
     */
    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return this.waitForPetToLoad()
            .pipe(
                switchMap(( loaded ) => {
                    // if ( !loaded ) {
                    //     this.store.dispatch( new fromStore.LoadPet( route.params.id ) );
                    // }
                    return this.hasPetDetails( route.params.id );
                }),
                catchError(() => of(false)),
            );
    }
}
