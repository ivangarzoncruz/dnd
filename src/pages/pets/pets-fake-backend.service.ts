import { InMemoryDbService } from "angular-in-memory-web-api";

export class PetsFakeBackend implements InMemoryDbService {
  createDb() {
    const pets = [
        {
            id            : 1,
            name          : "Lukas",
            breed         : "Cavoodle",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/cavoodle-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
        {
            id            : 2,
            name          : "Pitin",
            breed         : "Pembroke Welsh Corgi",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/welsh-corgy-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
        {
            id            : 3,
            name          : "Joe",
            breed         : "Jack Russell",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/jack-russell-terrier-smooth-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
        {
            id            : 4,
            name          : "Choky",
            breed         : "Beagle",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/beagle-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
        {
            id            : 5,
            name          : "Lisa",
            breed         : "Yorkshire Terrier",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/Yorkshire-Terrier-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
        {
            id            : 6,
            name          : "Toby",
            breed         : "Toy Poodle",
            description   : "Among the most agreeable of all small housedogs, the Pembroke Welsh Corgi is a strong, athletic…",
            avatar        : "https://bowwowinsurance.com.au/wp-content/uploads/2014/12/white-toy-poodle-sitting-isolated-700x700.jpg",
            type          : {
                id: 1,
                name: "Dog",
            },
        },
    ];

    return { pets };
  }
}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
