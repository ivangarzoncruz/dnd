import { IType as Type } from "./type.model";
export interface IPet {
    id: string;
    name: string;
    breed: string;
    description: string;
    type?: Type;
    avatar: string;
}
