import { IType as Type } from "./type.model";

export interface IQuery {
    search: string;
    sort: {
        type: string;
        order: string;
    };
    type?: Type;
}
