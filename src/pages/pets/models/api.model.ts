// // api.model.ts
// namespace ApiModel {
//     export interface IPet {
//         id: string;
//         name: string;
//         breed: string;
//         description: string;
//         type?: IType;
//         avatar: string;
//     }

//     export interface IQuery {
//         search: string;
//         sort: {
//             type: string;
//             order: string;
//         };
//         type?: IType;
//     }

//     export interface IType {
//         id?: number;
//         name: string;
//         status: string;
//     }
// }

// // using the interfaces
// // export class MyComponent {
// //     pet: ApiModel.Ipet;
// //     query: ApiModel.IQuery;
// // }

