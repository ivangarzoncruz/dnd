export interface IType {
    id?: number;
    name: string;
    status: string;
}
