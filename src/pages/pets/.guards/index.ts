import { PetsGuard } from "./pets.guard";
import { PetExistsGuard } from "./pet-exists.guard";
import { PetDetailsExistsGuard } from "./pet-details-exists.guard";

export const guards: any[] = [
    PetsGuard,
    PetExistsGuard,
    PetDetailsExistsGuard,
];

export * from "./pets.guard";
export * from "./pet-exists.guard";
export * from "./pet-details-exists.guard";
