import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { tap, filter, take, switchMap, catchError } from "rxjs/operators";

import * as fromStore from "../store";

@Injectable()
export class PetsGuard implements CanActivate {

    constructor(private store: Store<fromStore.PetsState>) {}

    canActivate(): Observable<boolean> {
        return this.checkStore().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false)),
        );
    }

    /**
     * Preload data from API if we haven"t visited before,
     * Otherwise, use existing state in store.
     *
     * @param {}
     * @returns Observable
     */
    checkStore(): Observable<boolean> {
        return this.store.select( fromStore.getPetsLoaded ).pipe(
            tap( ( loaded ) => {
                if ( !loaded ) {
                    this.store.dispatch( new fromStore.LoadPets() );
                }
            }),
            filter( (loaded) => loaded),
            take(1),
        );
    }
}
