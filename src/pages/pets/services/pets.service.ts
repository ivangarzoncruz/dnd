// Imports
import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs";
import { Subject } from "rxjs/Subject";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { IPet as Pet } from "./../models/pet.model";
import { IQuery as Query } from "./../models/query.model";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
};

// Decorator to tell Angular that this class can be injected as a service to another class
@Injectable()
export class PetsService {

    private url: string;
    private urlMatcher: RegExp;

    // Class constructor with Jsonp injected
    constructor( @Inject(HttpClient) private http: HttpClient  ) {

        // Base URL for Petfinder API
        this.url = "api/pets";
        this.urlMatcher = /\/api\/pets\/([0-9]+)/i;
    }

    /**
     * @method GET
     * @description GET all pets
     * @example /pets
     * @returns {Observable<Pet>} The pet observable.
     */
    public getPets(): Observable<Pet[]> {

        const url = `${ this.url }`;

        return this.http
            .get( url, httpOptions )
            .map( this.onGetUsersSuccess )
            .catch( this.onGetUsersError );
            // return this.http.get<Pet[]>(this.url);
    }

    /**
     * @method DELETE
     * @param {payload} Pet Pet object
     * @description DELETE pet given id
     * @example /pets/:id
     * @returns {Observable<Pet>} The pet observable.
     */
    public removePet( payload: Pet ): Observable<Pet> {

        const id = typeof payload === "number" ? payload : payload.id;
        const url = `${ this.url }/${ id }`;

        return this.http.delete<Pet>( url, httpOptions );

        // return this.http
        //     .delete( url, httpOptions )
        //     .map( this.onGetUsersSuccess )
        //     .catch( this.onGetUsersError );
    }

    /**
     * @method GET
     * @param {query} Query Query object
     * @description  GET pets whose name are contain in search term
     * @example /pets/?name=${ query.search }
     * @returns {Observable<Pet>} The pet observable.
     */
    public searchPets( query: Query ): Observable<Pet[]> {

        // if ( !term.trim() ) {
        if ( !query ) {

            // if not search term, return empty hero array.
            return of([]);
        }

        const url = `${ this.url }/?name=${ query.search }`;

        return this.http.get<Pet[]>( url, httpOptions );
        // return this.http.get<Pet[]>( url ).pipe(
        //     tap( _ => this.log(`found heroes matching "${term}"`)),
        //     catchError(this.handleError<Hero[]>('searchHeroes', []))
        // );
    }

    /**
     * @method GET
     * @param {payload} payload payload object
     * @description  GET pet matching the given id
     * @example /pets/:id
     * @returns {Observable<Pet>} The pet observable.
     */
    public getPetById( payload: number ) {

        console.log( "payload ----> ", payload );

        // const id = typeof payload === "number" ? payload : payload.id;
        const id = typeof payload === "number" ? payload : 1;
        const url = `${ this.url }/${ id }`;

        return this.http
            .get( url, httpOptions )
            .map( this.onGetUsersSuccess )
            .catch( this.onGetUsersError );

        // url.match( this.urlMatcher ) );
    }

    /**
     * @method POST
     * @param {payload} payload payload object
     * @description  POST add a new pet to the server
     * @example /pets
     * @returns {Observable<Pet>} The pet observable.
     */
    public createPet( payload: Pet ): Observable<Pet> {
        const url = `${ this.url }`;
        return this.http.post<Pet>( url, payload, httpOptions );
    }

    // Update a pet
    // POST: /pets
    // update(user: User) {
    //     return this.http.put(`${config.apiUrl}/users/` + user.id, user);
    // }

    private onGetUsersSuccess( response: Response ) {
        const body: any = response;
        return body || {};
    }

    private onGetUsersError( error: any ): Observable<Pet> {
        // console.error( "There was an error : " + err);
        const message: string = ( error.message ) ? error.message : error.status ? `${ error.status } - ${ error.statusText }` : "Server error";
        return Observable.throw( message );
    }
}
