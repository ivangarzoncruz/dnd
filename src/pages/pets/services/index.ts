// import { PetsFakeBackendService } from "./pets-fake-backend.service";
import { PetsService } from "./pets.service";

export const services: any[] = [
    // PetsFakeBackendService,
    PetsService,
];

export * from "./pets.service";
// export * from "./pets-fake-backend.service";
