import { Component, Inject, HostListener, VERSION, OnInit, ElementRef } from "@angular/core";
import { DOCUMENT } from "@angular/common";

@Component({
    moduleId    : module.id,
    selector    : "app-home",
    templateUrl : "./home.component.html",
    // styleUrls   : [ "./home.component.scss".toString() ],
})
export class HomeComponent {
    public home: object = {};
    public version: string;
    public greeting: string;
    // public navIsFixed: boolean;

    constructor(@Inject(DOCUMENT) private document: Document, public el: ElementRef) {
        this.version = `Angular! v${VERSION.full}`;
        this.greeting = "Welcome";
    }

    private ngOnInit(): void {
        this.home = {
          title : "Welcome to Angular 2 by Ivancho",
          banner : "./assets/images/cover.jpg",
        };
    }

    private toggleGreeting() {
        this.greeting = this.greeting === "Welcome" ? "What's up" : "Welcome";
    }
}
