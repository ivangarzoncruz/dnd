const path      = require( 'path' );
const fs        = require( 'fs' );
const webpack   = require( 'webpack' );
const rules     = require( './webpack/rules' );

/*------------------------------*/
/*  WEBPACK PLUGINS             */
/*------------------------------*/
const ContextReplacementPlugin  = require( 'webpack/lib/ContextReplacementPlugin' );
const HtmlWebpackPlugin         = require( 'html-webpack-plugin' );
const MiniCssExtractPlugin      = require( 'mini-css-extract-plugin' );
const CleanWebpackPlugin        = require( 'clean-webpack-plugin' );
const BannerWebpackPlugin       = require( 'webpack/lib/BannerPlugin' );

/*------------------------------*/
/*  THIRD PARTS PLUGINS         */
/*------------------------------*/
const ExtractTextPlugin     = require( 'extract-text-webpack-plugin' );
const BrowserSyncPlugin     = require( 'browser-sync-webpack-plugin' );

/*------------------------------*/
/*  WEBPACK CONSTANTS           */
/*------------------------------*/
// const ENV = process.env.ENV = process.env.NODE_ENV = 'development';
// const HOST = process.env.HOST || 'localhost';
// const PORT = process.env.PORT || 9020;
// const HMR = helpers.hasProcessFlag('hot');

// const METADATA = webpackMerge( commonConfig.metadata, {
//   host: HOST,
//   port: PORT,
//   ENV: ENV,
//   HMR: HMR
// });

const NODE_ENV = process.env.NODE_ENV;

/*------------------------------*/
/*  WEBPACK HELPERS           */
/*------------------------------*/
const setPath = function( folderName ) {
    return path.join( __dirname, folderName );
}

const buildingForLocal = function() {
    return ( NODE_ENV === 'development' );
};

const setPublicPath = function() {
    let env = NODE_ENV;
    if (env === 'production') {
        return 'https://your-directory/production/';
    } else if ( env === 'staging' ) {
        return 'https://your-directory/staging/';
    } else {
        return '/';
    }
};

const config = {
    /**
     * You can use these too for bigger projects. For now it is 0 conf mode for me!
     */
    entry: {
        polyfills : path.join(setPath( './src' ), 'polyfills.ts'),
        main      : path.join(setPath( './src' ), 'main.ts'),
    },

    output: {
        path          : buildingForLocal() ? path.resolve( __dirname ) : setPath( 'dist' ), // this one sets the path to serve
        publicPath    : setPublicPath(),
        filename      : buildingForLocal() ? '[name].js' : '[name].js'
    },

    // optimization:{
    //     runtimeChunk: false,
    //     splitChunks: {
    //         chunks: "all", //Taken from https://gist.github.com/sokra/1522d586b8e5c0f5072d7565c2bee693
    //     }
    // },

    resolve: {

        /*
         * An array of extensions that should be used to resolve modules.
         * See: http://webpack.github.io/docs/configuration.html#resolve-extensions
         */
        extensions: [ '.ts', '.tsx', '.js', '.jsx', '.json', '.css', '.sass', '.scss', '.html' ],

        modules: [
            setPath( 'src' ),
            setPath( 'node_modules' )
        ]
    },

    // resolveLoader: {
    //     modules: [ setPath('node_modules') ]
    // },

    mode: buildingForLocal() ? 'development' : 'production',

    devServer: {
        // contentBase: "/", //本地服务器所加载的页面所在的目录
        inline : true,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/
        },
        // noInfo : false,
        port: 9020,
        // host: METADATA.host,
        historyApiFallback: true,
    },

    plugins: [

        new HtmlWebpackPlugin({
            title: 'History Search',
            filename: 'index.html',
            inject: true,
            template: setPath( 'src/index.ejs' ),
            minify: {
                removeAttributeQuotes: false,
                collapseWhitespace: false,
                html5: true,
                minifyCSS: false,
                removeComments: false,
                removeEmptyAttributes: false
            },
            environment: process.env.NODE_ENV,
            isLocalBuild: buildingForLocal(),
            imgPath: (!buildingForLocal()) ? 'assets' : 'src/assets'
        }),

        new BannerPlugin( 'Iv@ncho 2018' ),

        // new CleanWebpackPlugin( 'dist', {} ),

        // new MiniCssExtractPlugin({
        //     // Options similar to the same options in webpackOptions.output
        //     // both options are optional
        //     filename: "assets/css/[name].css",
        //     chunkFilename: "[id].css"
        // }),

        new webpack.DefinePlugin({
            'process.env': {
                isStaging   : ( NODE_ENV === 'development' || NODE_ENV === 'staging' ),
                NODE_ENV    : '"' + NODE_ENV + '"'
            }
        }),

        new ExtractTextPlugin( 'assets/css/[name].css' ),

        // Workaround for angular/angular#11580
        new ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)(@angular|esm5)/,
            setPath( 'src' ), // location of your source
            {} // a map of your routes
        ),

        // new webpack.NamedModulesPlugin(),
		// new webpack.optimize.OccurrenceOrderPlugin(),
		// new webpack.HotModuleReplacementPlugin(),

        // new BrowserSyncPlugin({
        //     host: 'localhost',
        //     port: 9020,
        //     server: {
        //       baseDir: setPath( '/' )
        //     },
        //     ui: false,
        //     online: false,
        //     notify: false
        // }),
    ],

    module: {

        /*
        * An array of automatically applied loaders.
        *
        * IMPORTANT: The loaders here are resolved relative to the resource which they are applied to.
        * This means they are not resolved relative to the configuration file.
        *
        * See: http://webpack.github.io/docs/configuration.html#module-loaders
        */
        rules : rules
    },
};
module.exports = config;
