const path      = require( 'path' );
const helpers   = require( './helpers' );

const ExtractTextPlugin     = require( 'extract-text-webpack-plugin' );
const MiniCssExtractPlugin  = require( 'mini-css-extract-plugin' );

// console.log( '^_^____________________________________^_^' );
// console.log( 'helpers root ----> ', helpers.root( 'src' ) );
// console.log( '^_^____________________________________^_^' );

module.exports = [
    {
        test: /\.ts(x?)$/,
        use: [ 'awesome-typescript-loader', 'angular2-template-loader' ],
        include: [ helpers.root( 'src' ) ],
        exclude: [ /\.(spec|e2e)\.ts$/, helpers.root( 'node_modules' ) ]
    },
    {
        test: /\.js$/,
        use: [
            {
                loader: "babel-loader",
                options: { presets: ['es2015'] }
            }
        ],
        exclude: /(node_modules|bower_components)/,
    },
    {
        test: /\.html$/,
        use: 'raw-loader',
    },
    {
        test: /\.css$/,
        use: [ MiniCssExtractPlugin.loader, "css-loader" ]
    },
    {
        test: /\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [ 'css-loader', 'sass-loader' ]
        }),
        // use: [
        //     {
        //       loader: "style-loader" // creates style nodes from JS strings
        //     },
        //     {
        //       loader: "css-loader" // translates CSS into CommonJS
        //     },
        //     {
        //       loader: "sass-loader" // compiles Sass to CSS
        //     }
        // ],
        // use:  [  'style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader' ],
        exclude: [ helpers.root( 'node_modules' ) ]
    },
    {
        test: /\.svg$/,
        loader: 'svg-sprite-loader'
    },
    {
        test: /\.(woff|woff2|eot|ttf|svg)?(\?v=[0–9]\.[0–9]\.[0–9])?$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: 'assets/fonts/[name].[ext]?[hash]',
                    // useRelativePath: buildingForLocal()
                },
            }
        ],
    },
    {
        test: /\.(png|gif|jpe?g|ico)$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]',
                    // useRelativePath: buildingForLocal()
                },
            }
        ],
        exclude: [ helpers.root( 'node_modules' ) ]
    }
];
